from typing import Dict, List, Literal, Union
from uuid import uuid4, UUID

from pydantic import BaseModel, Field, NonNegativeInt

from ska_src_data_management_api.common import constants
from ska_src_data_management_api.models.schema import Schema


# Base Response Model
class Response(BaseModel):
    """Base response model for all API responses."""
    pass


# Data Access Responses
class DataAccessForServiceResponse(Response):
    """Response model for data access service."""
    address: str = Field(examples=["http://service.srcdev.skao.int:443"])
    access_token: str = Field(examples=["ejy..."])


class DataDownloadResponse(Response):
    """Response model for data download service."""
    access_token: str = Field(examples=["ejy..."])


# Data List Responses
DataListIdentifiersResponse = List[str]
DataListNamespacesResponse = List[str]


class DataListRulesNamespaceResponse(Response):
    """Response model for listing rules in a namespace."""
    truncated: bool = Field(examples=[True])
    count: int = Field(examples=[100])
    rules: List[Dict]


DataListRulesNamespaceNameResponse = List[Dict]


# Data Location Responses
class DataLocateReplicasResponse(Response):
    """Response model for locating replicas."""
    identifier: str = Field(examples=["STFC_STORM"])
    associated_storage_area_id: UUID = Field(default_factory=uuid4)
    replicas: List[str]


class DataLocateReplicasWithServicesResponse(DataLocateReplicasResponse):
    """Response model for locating replicas with services."""
    colocated_services: List


# Data Movement Responses
class DataMoveResponse(Response):
    """Response model for data move operation."""
    job_id: str = Field(examples=['3a12b287-d3ec-43fe-9b5b-be8acf3a3528'])


class DataMoveStatusResponse(Response):
    """Response model for the status of a data move operation."""
    state: constants.DataMovementStatus


# Data Removal Responses
class DataRemoveResponse(Response):
    """Response model for data removal operation."""
    acknowledged: bool = Field(examples=[True])


# Data Staging Responses
class DataStageResponse(Response):
    """Response model for data stage operation."""
    job_id: str = Field(examples=['3a12b287-d3ec-43fe-9b5b-be8acf3a3528'])


class DataStageStatusResponse(Response):
    """Response model for the status of a data move operation."""
    state: constants.DataMovementStatus


# Data Upload Responses
class DataUploadIngestResponse(Response):
    """Response model for data upload and ingest operation."""
    access_token: str = Field(examples=["ejy..."])


# Generic Error Responses
class GenericErrorResponse(Response):
    """Response model for generic errors."""
    detail: str


class GenericOperationResponse(Response):
    """Response model for generic operation success."""
    successful: bool = Field(examples=[True])


class GenericRucioErrorResponse(Response):
    """Response model for Rucio-specific errors."""
    detail: str


# Health Check Responses
class HealthResponse(Response):
    """Response model for health check endpoint."""
    class DependentServices(BaseModel):
        """Model for dependent services status."""
        class DependentServiceStatus(BaseModel):
            """Model for individual dependent service status."""
            status: Literal["UP", "DOWN"] = Field(examples=["UP"])

        rucio: DependentServiceStatus = Field(alias="rucio")
        permissions_api: DependentServiceStatus = Field(alias="permissions-api")
        site_capabilities_api: DependentServiceStatus = Field(alias="site-capabilities-api")

    uptime: NonNegativeInt = Field(examples=[1000])
    number_of_managed_requests: NonNegativeInt = Field(examples=[50])
    dependent_services: DependentServices


# Metadata Responses
MetadataGetResponse = Dict[str, Union[float, str]]


class MetadataSetResponse(Response):
    """Response model for setting metadata."""
    pass


# Ping Response
class PingResponse(Response):
    """Response model for ping status."""
    status: Literal["UP", "DOWN"]
    version: str


# Schema Responses
SchemasResponse = List[str]
SchemaGetResponse = Schema
