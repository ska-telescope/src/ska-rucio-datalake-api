import ast
import json
import time

import redis

from ska_src_data_management_api.cache.cache import Cache


class RedisCache(Cache):
    def __init__(self, host, port, logger=None):
        """ Cache for tokens.

        An entry in the cache takes the format of:

        (scopes_1): token_1,
        (scopes_2): token_2,
        ...
        """
        self.logger = logger
        self.redis = redis.Redis(host=host, port=port, decode_responses=True)

    def get_cache_token(self, scopes, audiences, key_prefix=None):
        if key_prefix:
            # sorted by alphabetical order
            key = '{}_{}_{}'.format(
                key_prefix, repr(tuple(sorted(scopes.split()))), repr(tuple(sorted(audiences.split()))))
        else:
            # sorted by alphabetical order
            key = '{}_{}'.format(repr(tuple(sorted(scopes.split()))), repr(tuple(sorted(audiences.split()))))
        entry = self.redis.get(key)
        if entry:
            token = json.loads(entry)
            if int(time.time()) < token['expires_at'] - 60:
                return token
            else:
                self.redis.delete(key)
                if self.logger:
                    self.logger.info("Cached token has expired.")
        return None

    def set_cache_token(self, scopes, audiences, token, key_prefix=None):
        if key_prefix:
            # sorted by alphabetical order
            key = '{}_{}_{}'.format(
                key_prefix, repr(tuple(sorted(scopes.split()))), repr(tuple(sorted(audiences.split()))))
        else:
            # sorted by alphabetical order
            key = '{}_{}'.format(repr(tuple(sorted(scopes.split()))), repr(tuple(sorted(audiences.split()))))
        self.redis.set(key, json.dumps(token))
