from abc import ABC


class Cache(ABC):
    def get_cache_token(self, scopes, key_prefix=None):
        raise NotImplementedError

    def set_cache_token(self, scopes, token, key_prefix=None):
        raise NotImplementedError

