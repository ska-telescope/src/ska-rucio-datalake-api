import os
import json
import logging
import math
import requests
import time
import uuid
from typing import List, Optional, Union
from urllib.parse import urlparse

import asyncio
from celery import Celery
from collections import Counter

from ska_src_data_management_api.auth.oauth2.oauth2 import OAuth2ServiceTokenFactory
from ska_src_data_management_api.cache.redis import RedisCache
from ska_src_data_management_api.common import constants
from ska_src_data_management_api.common.utility import retry_request, retry_request_streamed_response
from ska_src_data_management_api.common.exceptions import PrepareDataNotFound
from ska_src_data_management_api.session import AuthenticatedRucioOAuth2Session, \
    AuthenticatedSiteCapabilitiesAPIOAuth2Session
from ska_src_data_management_api.tasks.celery import celery


@celery.task(bind=True)
def move_data(self,
              storage_area_identifier: str,
              parent_namespace: Optional[Union[str, None]],
              lifetime: int,
              dids: list,
              rucio_host_url: str,
              rucio_account: str,
              rucio_admin_client_id: str,
              rucio_admin_client_secret: str,
              rucio_admin_client_scopes: str,
              rucio_admin_client_audience: str,
              iam_token_endpoint: str,
              cache_type: Optional[Union[str, None]],
              cache_host: Optional[Union[str, None]],
              cache_port: Optional[Union[int, None]],
              poll_rule_loop_for_total_s=86400,
              poll_rule_loop_delay_s=10
              ):
    """ Request movement of existing data to another storage area by addition of replication rules to a set of data
    identifiers if required. """
    # Set logging to use uvicorn logger.
    #
    logger = logging.getLogger("uvicorn")

    # Instantiate token cache if requested.
    #
    cache = None
    if cache_type == 'redis':
        cache = RedisCache(host=cache_host, port=cache_port, logger=logger)

    # Instantiate a service token factory (OAuth2).
    #
    service_token_factory = OAuth2ServiceTokenFactory(
        rucio_admin_client_params={
            'client_id': rucio_admin_client_id,
            'client_secret': rucio_admin_client_secret,
            'client_scopes': rucio_admin_client_scopes,
            'client_audience': rucio_admin_client_audience,
            'iam_token_endpoint': iam_token_endpoint
        },
        cache=cache,
        logger=logger
    )
    # Instantiate an authenticated Rucio session.
    #
    rucio_session = AuthenticatedRucioOAuth2Session(service_token_factory=service_token_factory)

    # Get Rucio constants.
    #
    rucio_constants = constants.Rucio(rucio_host_url=rucio_host_url)

    # To move data, we attach data to a parent identifier with the following nested structure capable of holding all
    # the possible native data types:
    #
    # parent_container
    # |_ parent_container.containers (CONTAINER)
    # |_ parent_container.datasets (CONTAINER)
    # |_ parent_container.files (DATASET)
    #
    # To conform with the requirements that DATASETs contain only FILEs and CONTAINERS contain both DATASETs and other
    # CONTAINERs but not FILEs.
    #
    # First, create this parent data container identifier. Since data identifiers to move could be specified across
    # namespaces, we use the first did's namespace. This can be overridden by specifying the parent_namespace parameter.
    #
    total_elapsed_st = time.time()

    # Update the internal celery task state (WRAPPING).
    self.update_state(
        state=constants.DataMovementStatus.WRAPPING,
        meta={
            'total_elapsed_s': round(time.time() - total_elapsed_st, 1)
        }
    )

    if not parent_namespace:
        parent_namespace = dids[0].get('namespace')
    parent_name = str(uuid.uuid4())

    # Prepare add DID request and send.
    #
    method, url, headers = rucio_constants.rucio_endpoint_add_did
    url = url.format(scope_name="{}/{}".format(parent_namespace, parent_name))
    data = json.dumps({
        "type": "CONTAINER",
        "lifetime": lifetime
    }).encode('utf-8')
    retry_request(method, url, headers, data=data, session=rucio_session.get_sync())

    # Next, for each considered DID we get what type it is (FILE, CONTAINER, DATASET) so we can attach it to the correct
    # child container/dataset.
    #
    containers = []
    datasets = []
    files = []
    for did in dids:
        # Prepare get DID request and send.
        #
        method, url, headers = rucio_constants.rucio_endpoint_get_did
        url = url.format(scope_name="{}/{}".format(did.get('namespace'), did.get('name')))
        response = retry_request(method, url, headers, session=rucio_session.get_sync())

        # Filter by type into the different lists.
        did_type = json.loads(response.text).get('type')
        if did_type == 'FILE':
            files.append(did)
        elif did_type == 'DATASET':
            datasets.append(did)
        elif did_type == 'CONTAINER':
            containers.append(did)

    # Create the child datasets and containers (complete with contents) and attach to the parent container.
    #
    # Define url and populate Rucio token header with exchanged token.
    for contents, suffix, did_type in zip([containers, datasets, files], ['containers', 'datasets', 'files'],
                                          ["CONTAINER", "CONTAINER", "DATASET"]):
        child_namespace = parent_namespace
        child_name = "{}.{}".format(parent_name, suffix)

        # Create the child container/dataset DID and attach respective contents to this child container/dataset.
        #
        method, url, headers = rucio_constants.rucio_endpoint_add_did
        url = url.format(scope_name="{}/{}".format(child_namespace, child_name))
        data = json.dumps({
            "type": did_type,
            "lifetime": lifetime,
            "dids": [{'scope': entry.get('namespace'), 'name': entry.get('name')} for entry in contents]
        }).encode('utf-8')
        retry_request(method, url, headers, data=data, session=rucio_session.get_sync())

        # Attach this child dataset/container to the parent container.
        #
        method, url, headers = rucio_constants.rucio_endpoint_attach_dids
        url = url.format(scope_name="{}/{}".format(parent_namespace, parent_name))
        data = json.dumps({
            "dids": [{
                'scope': child_namespace,
                'name': child_name
            }]
        }).encode('utf-8')
        retry_request(method, url, headers, data=data, session=rucio_session.get_sync())

    # Add rule to parent dataset.
    #
    method, url, headers = rucio_constants.rucio_endpoint_add_rules
    data = json.dumps({
        "account": rucio_account,
        "dids": [{
            'scope': parent_namespace,
            'name': parent_name
        }],
        "copies": 1,
        "rse_expression": storage_area_identifier,
        "lifetime": lifetime
    }).encode('utf-8')
    response = retry_request(method, url, headers, data=data, session=rucio_session.get_sync())

    # Get the job id. In this context, the job id is just the replication rule id for the parent container.
    #
    job_id = json.loads(response.text)[0]

    # Poll the Rucio API for the rule status.
    #
    current_job_status = constants.DataMovementStatus.UNKNOWN
    current_iteration = 1
    loop_elapsed_st = last_iteration_time = time.time()
    while time.time() - loop_elapsed_st < poll_rule_loop_for_total_s:
        if current_iteration == 1 or time.time() - last_iteration_time > poll_rule_loop_delay_s:
            last_iteration_time = time.time()

            # Prepare get rule request and send.
            #
            method, url, headers = rucio_constants.rucio_endpoint_get_rule
            url = url.format(rule_id=job_id)
            response = retry_request(method, url, headers, data=data, session=rucio_session.get_sync())

            # Handle the rule state.
            #
            rule = json.loads(response.text)
            rule_id = rule.get('id')
            last_reported_rule_state = rule.get('state')
            if last_reported_rule_state == constants.RucioRuleStates.OK:
                current_job_status = constants.DataMovementStatus.READY
            elif last_reported_rule_state == constants.RucioRuleStates.REPLICATING:
                current_job_status = constants.DataMovementStatus.MOVING
                # Update the internal celery task state (MOVING).
                self.update_state(
                    state=current_job_status,
                    meta={
                        'total_elapsed_s': round(time.time() - total_elapsed_st, 1),
                        'poll_rule_loop_elapsed_s': round(time.time() - loop_elapsed_st, 1),
                        'n_iterations': current_iteration,
                        'rule_id': rule_id,
                        'last_reported_rule_state': last_reported_rule_state
                    }
                )
            elif last_reported_rule_state == constants.RucioRuleStates.STUCK:
                current_job_status = constants.DataMovementStatus.ERROR
            else:
                current_job_status = constants.DataMovementStatus.UNKNOWN
                # Update the internal celery task state (UNKNOWN).
                self.update_state(
                    state=current_job_status,
                    meta={
                        'total_elapsed_s': round(time.time() - total_elapsed_st, 1),
                        'poll_rule_loop_elapsed_s': round(time.time() - loop_elapsed_st, 1),
                        'n_iterations': current_iteration,
                        'rule_id': rule_id,
                        'last_reported_rule_state': last_reported_rule_state
                    }
                )

            # Break out of polling if the movement job is one that implies the job is "complete".
            #
            if current_job_status in (constants.DataMovementStatus.READY,
                                      constants.DataMovementStatus.ERROR):
                break

            current_iteration += 1
    final_job_status = current_job_status

    return {
        'status': final_job_status,
        'total_elapsed_s': round(time.time() - total_elapsed_st, 1),
        'container_did': "{}:{}".format(parent_namespace, parent_name),
        'storage_area_identifier': storage_area_identifier
    }


@celery.task(bind=True)
def prepare_data(self,
                 container_did_sa_identifier: tuple,
                 storage_area_props: dict,
                 preparation_strategy: constants.DataPreparingStrategies, # TODO: Remove (deprecated)
                 user_authz_header: str,
                 did_to_relpath_mappings: list,
                 rucio_host_url: str,
                 rucio_admin_client_id: str,
                 rucio_admin_client_secret: str,
                 rucio_admin_client_scopes: str,
                 rucio_admin_client_audience: str,
                 iam_token_endpoint: str,
                 cache_type: Optional[Union[str, None]],
                 cache_host: Optional[Union[str, None]],
                 cache_port: Optional[Union[int, None]],
                 poll_pd_delay_s=2,
                 poll_pd_retries=10
                 ):
    """ Prepare data in the environment by calling the preparation service at a node. """
    # Set logging to use uvicorn logger.
    #
    logger = logging.getLogger("uvicorn")

    # Unpack values from move_data:
    #
    container_did, storage_area_identifier = container_did_sa_identifier

    # Instantiate token cache if requested.
    #
    cache = None
    if cache_type == 'redis':
        cache = RedisCache(host=cache_host, port=cache_port, logger=logger)

    # Instantiate a service token factory (OAuth2).
    #
    service_token_factory = OAuth2ServiceTokenFactory(
        rucio_admin_client_params={
            'client_id': rucio_admin_client_id,
            'client_secret': rucio_admin_client_secret,
            'client_scopes': rucio_admin_client_scopes,
            'client_audience': rucio_admin_client_audience,
            'iam_token_endpoint': iam_token_endpoint
        },
        cache=cache,
        logger=logger
    )

    # Instantiate an authenticated Rucio session.
    #
    rucio_session = AuthenticatedRucioOAuth2Session(service_token_factory=service_token_factory)

    # Get Rucio and PrepareData constants.
    #
    rucio_constants = constants.Rucio(rucio_host_url=rucio_host_url)

    try:
        prepdata_constants = constants.PrepareData(
            prepare_data_url=storage_area_props[storage_area_identifier]["preparedata_url"],
            user_authz_header=user_authz_header
        )
    except KeyError as e:
        raise PrepareDataNotFound(
            "No prepare data service found for storage area {}".format(storage_area_identifier)
        ) from e

    total_elapsed_st = time.time()
    if not container_did:
        logger.critical("No container_did passed to prepare_data")
        self.update_state(
            state=constants.DataPreparingStatus.ERROR,
            meta={
                'total_elapsed_s': round(time.time() - total_elapsed_st, 1),
                'container_did': container_did
            }
        )
        final_job_additional_info = {
            'reason': "No container_did passed to prepare_data"
        }
    else:
        # First verify that the rule corresponding to the container DID is in an OK state
        # at the requested node.
        #
        # Update the internal celery task state (VERIFYING).
        self.update_state(
            state=constants.DataPreparingStatus.VERIFYING,
            meta={
                'total_elapsed_s': round(time.time() - total_elapsed_st, 1),
                'container_did': container_did
            }
        )

        container_namespace, container_name = container_did.split(':')

        # Prepare list rules request and send.
        #
        method, url, headers = rucio_constants.rucio_endpoint_list_rules
        url = url.format(scope=container_namespace, name=container_name)
        rules_response = retry_request(method, url, headers, session=rucio_session.get_sync())

        rules = json.loads(rules_response.text)
        rules = [rules] if not isinstance(rules, list) else rules
        found_rule = False
        for rule in rules:
            if rule.get('rse_expression') == storage_area_identifier and \
                    rule.get('state') == constants.RucioRuleStates.OK:
                # Verified, move to preparing.
                #
                # Update the internal celery task state (PREPARING).
                self.update_state(
                    state=constants.DataPreparingStatus.PREPARING,
                    meta={
                        'total_elapsed_s': round(time.time() - total_elapsed_st, 1)
                    }
                )
                found_rule = True

        final_job_additional_info = {}
        if found_rule:
            # Populate the request body with the following common parameters:
            #
            # - preparation strategy,
            # - user's access token from the authz header.
            #
            try:
                user_access_token = user_authz_header.split("Bearer ")[1]
            except (AttributeError, IndexError):
                user_access_token = ""
            common_data = {
                "user_access_token": user_access_token,
                "preparation_strategy": preparation_strategy
            }

            # Preparation strategy dependent additional request body.
            #
            if preparation_strategy == constants.DataPreparingStrategies.SYMLINKS:
                # The following makes no distinction for whether a file is nested in a container or datasets, it
                # simply finds every file it can and tries to map this file to a relative path using the
                # <did_to_relpath_mappings>
                #
                # First, do a recursive search on the container ID for datasets.
                #
                method, url, headers = rucio_constants.rucio_endpoint_get_did
                
                # Do a recursive search within container and create file list.
                #
                dids_response = retry_request(
                    "GET",
                    "{}/dids/{}?recursive=true&name={}".format(
                        rucio_host_url.rstrip("/"),
                        container_namespace,
                        container_name
                    ),
                    headers,
                    session=rucio_session.get_sync()
                )
                file_did_list = []
                try:
                    for did_str in dids_response.text.splitlines():
                        json_data = json.loads(did_str)
                        if json_data.get("type") == "FILE":
                            file_did_list.append(json_data)
                except json.JSONDecodeError as e:
                    logger.warning(f"Error decoding JSON in line: {did_str}")
                    logger.warning(f"Error: {e}")
                    raise

                # Get the replica location for this storage area identifier, for each file.
                #
                method, url, headers = rucio_constants.rucio_endpoint_list_replicas
                data = json.dumps({
                    "dids": [
                        {"scope": file["scope"], "name": file["name"]} for file in file_did_list
                    ],
                    "rse_expression": storage_area_identifier
                }).encode("utf-8")
                replicas_response = retry_request(
                    method, url, headers, session=rucio_session.get_sync(), data=data
                )
                # Each line in response is a string repr of a json object, partial structure e.g.:
                # {"scope":str, "name":str, "rses":{"rse_name":["pfn"]}}
                #
                # Parse this response into a list of dicts storing each file DID and PFN
                #
                replica_str_list = [
                    line.strip() for line in replicas_response.text.splitlines() if line.strip()
                ]
                replica_objs = [json.loads(replica_str) for replica_str in replica_str_list]

                # Create a dict of file dids with the relative paths of each on the storage
                #
                file_did_pfn_map = []
                for replica in replica_objs:
                    storage_root = storage_area_props[storage_area_identifier]["storage_root"]
                    pfn = urlparse(next(iter(replica["rses"].values()))[0]).path
                    relative_pfn = os.path.relpath(pfn, storage_root)
                    file_did_pfn_map.append({
                        "scope": replica["scope"],
                        "name": replica["name"],
                        "pfn": relative_pfn
                    })

                did_pfn_lookup = {did["name"]: did["pfn"] for did in file_did_pfn_map}

                # NOTE: Currently pfn contains storage basepath too; may wish to strip this off
                # e.g. pfn could equal "/sa/deterministic/testing/00/00/testfile.txt"
                # where perhaps "/testing/00/00/testfile.txt" would be preferred.

                method, url, headers = prepdata_constants.prepare_data_stage

                prepdata_body = [
                    [
                        f"{did['namespace']}:{did['name']}",
                        did_pfn_lookup[did["name"]],
                        did["relative_path"]
                    ]
                    for did in did_to_relpath_mappings
                ]
                # TODO: This could be a better format body for /preparedata calls.
                # Retaining for future when prepareData implementation is updated:
                #
                # prepdata_body = {
                #         "dids": [f"{did['namespace']}:{did['name']}", ...],
                #         "rel_path_src_files": [did_name_lookup[did["name"]],...],
                #         "rel_path_dst_dir_user_areas": [os.path.join(did["relative_path"], did["name"]),...],
                #     }
                #     for did in did_to_relpath_mappings
                # ]

                # TODO: Does this need a session?
                logger.info("Sending request to prepareData {} with body {}".format(url, prepdata_body))
                prepdata_response = retry_request(method, url, headers, data=json.dumps(prepdata_body))
                if prepdata_response.status_code == 201:
                    task_id = prepdata_response.json().get("task_id")
                    logger.info("Prepare data task requested: {}".format(task_id))
                else:
                    final_job_status = constants.DataPreparingStatus.ERROR
                    final_job_additional_info = {
                        'reason': "Error from prepareData service: {}.".format(
                            prepdata_response.text
                        )
                    }
                    return {
                        'status': final_job_status,
                        'total_elapsed_s': round(time.time() - total_elapsed_st, 1),
                        **final_job_additional_info
                    }
                
                # Poll preparedata service until task is complete:
                #
                method, url, headers = prepdata_constants.prepare_data_status
                url = url.format(task_id=task_id)
                for attempt in range(poll_pd_retries):
                    prepdata_status_response = retry_request(method, url, headers)
                    
                    if prepdata_status_response.status_code != 200:
                        logger.critical(
                            "Prepare data error while polling task status {}".format(
                                prepdata_status_response.status_code
                            ))
                        final_job_status = constants.DataPreparingStatus.ERROR
                        final_job_additional_info = {
                            'reason': "Data was not in an OK state at the requested site."
                        }
                        break
                    
                    pd_response = prepdata_status_response.json()
                    logger.info(f"Attempt {attempt + 1}: Task {task_id} - Status: {pd_response['status']}")

                    if pd_response["status"] == "SUCCESS":
                        logger.info("Prepare data task completed successfully!")
                        final_job_status = constants.DataPreparingStatus.READY
                        break

                    time.sleep(poll_pd_delay_s)
                
                else:
                    final_job_status = constants.DataPreparingStatus.ERROR
                    final_job_additional_info = {
                        'reason': "Prepare data task did not complete in time."
                    }

        else:
            final_job_status = constants.DataPreparingStatus.ERROR
            final_job_additional_info = {
                'reason': "Data was not in an OK state at the requested site."
            }

    return {
        'status': final_job_status,
        'total_elapsed_s': round(time.time() - total_elapsed_st, 1),
        **final_job_additional_info
    }


@celery.task
def unpack_move_data_return_for_prepare_data(move_data_return):
    # prepare_data requires two values from the move_data return 
    #
    return move_data_return.get('container_did'), \
        move_data_return.get('storage_area_identifier')


@celery.task(bind=True)
def select_best_staging_sa(
    self,
    storage_area_identifier: Optional[str],
    dids: list,
    storage_area_props: dict,
    rucio_host_url: str,
    rucio_admin_client_id: str,
    rucio_admin_client_secret: str,
    rucio_admin_client_scopes: str,
    rucio_admin_client_audience: str,
    iam_token_endpoint: str,
    cache_type: Optional[Union[str, None]],
    cache_host: Optional[Union[str, None]],
    cache_port: Optional[Union[int, None]]
):
    # If storage_area_identifier is set, we can pass that to the next stage...
    #
    if storage_area_identifier:
        return storage_area_identifier
    # ... otherwise select a logical SA based on the current file locations

    # Set logging to use uvicorn logger.
    #
    logger = logging.getLogger("uvicorn")

    # Instantiate token cache if requested.
    #
    cache = None
    if cache_type == 'redis':
        cache = RedisCache(host=cache_host, port=cache_port, logger=logger)

    # Instantiate a service token factory (OAuth2).
    #
    service_token_factory = OAuth2ServiceTokenFactory(
        rucio_admin_client_params={
            'client_id': rucio_admin_client_id,
            'client_secret': rucio_admin_client_secret,
            'client_scopes': rucio_admin_client_scopes,
            'client_audience': rucio_admin_client_audience,
            'iam_token_endpoint': iam_token_endpoint
        },
        cache=cache,
        logger=logger
    )

    # Instantiate an authenticated Rucio session.
    #
    rucio_session = AuthenticatedRucioOAuth2Session(service_token_factory=service_token_factory)

    # Get Rucio constants.
    #
    rucio_constants = constants.Rucio(rucio_host_url=rucio_host_url)

    # Get the replica locations for each file.
    #
    logger.info("Getting file replica locations for {} dids".format(len(dids)))
    method, url, headers = rucio_constants.rucio_endpoint_list_replicas
    data = json.dumps({
        "dids": [
            {"scope": file["namespace"], "name": file["name"]} for file in dids
        ]
    }).encode("utf-8")

    replicas_response = retry_request(
        method, url, headers, session=rucio_session.get_sync(), data=data
    )

    # Each line in response is a string repr of a json object, partial structure e.g.:
    # {"scope":str, "name":str, "rses":{"RSE_NAME":["pfn"]}, "states":{"RSE_NAME":"AVAILABLE"}}
    # where RSE_NAME is the storage_area_identifier
    #
    # Parse this response into a list of dicts containing file DID states
    #
    replica_str_list = [
        line.strip() for line in replicas_response.text.splitlines() if line.strip()
    ]
    replica_objs = [json.loads(replica_str) for replica_str in replica_str_list]

    # Count up all the occurrences of each RSE_NAME showing a replica being AVAILABLE
    #
    sa_counts = Counter(
        sa for did in replica_objs for sa, state in did.get(
            "states", {}
        ).items() if state == "AVAILABLE"
    )

    # Get the most common storage_area_identifier
    #
    sa_max, count = sa_counts.most_common(1)[0] if sa_counts else (None, 0)
    logger.info("Most common storage_area_identifier was {} ({}/{} files available)".format(
        sa_max, count, len(dids)
    ))

    # Not all SAs have a prepare data service
    valid_sas = {
        sa: count for sa, count in sa_counts.items() if sa in storage_area_props
    }

    if valid_sas:
        # Pick the site with the highest replica count
        storage_area_identifier = max(valid_sas, key=valid_sas.get)
        logger.info(
            "storage_area_identifier with most files and a data preparation service " \
            "was {} ({}/{} files available)".format(
                storage_area_identifier, count, len(dids)
            )
        )
    else:
        # No files found at sites with PD services
        storage_area_identifier = list(storage_area_props.keys())[0]
        logger.info("No prepare data services at file DID sites. Selecting site {}".format(
            storage_area_identifier
        ))

    return storage_area_identifier