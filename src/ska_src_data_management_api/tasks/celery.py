import os

from celery import Celery

REDIS_CACHE_HOST = os.environ.get('REDIS_CACHE_HOST')
REDIS_CACHE_PORT = os.environ.get('REDIS_CACHE_PORT')
if not REDIS_CACHE_HOST and not REDIS_CACHE_PORT and os.environ.get('CACHE_TYPE', "") == 'redis':
    REDIS_CACHE_HOST = os.environ.get('REDIS_CACHE_HOST', os.environ.get('CACHE_HOST', 'localhost'))
    REDIS_CACHE_PORT = os.environ.get('REDIS_CACHE_PORT', os.environ.get('CACHE_PORT', '6379'))

celery = Celery(
    'ska_src_data_management_api.tasks',
    broker='redis://{}:{}/0'.format(REDIS_CACHE_HOST, REDIS_CACHE_PORT),
    backend='redis://{}:{}/0'.format(REDIS_CACHE_HOST, REDIS_CACHE_PORT)
)

celery.broker_connection_retry_on_startup = True
celery.autodiscover_tasks(['ska_src_data_management_api.tasks.data'])