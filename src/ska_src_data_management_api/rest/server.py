import ast
import copy
import dateparser
import io
import json
import os
import random
import logging
import pathlib
import pickle
import tempfile
import re
import time
import uuid
from collections import OrderedDict
from datetime import datetime
from typing import Dict, Optional, Union, List
from urllib.parse import parse_qsl, urlencode

import asyncio
import geoip2.database
import humanize
import jsonref
import jsonschema
import requests
from celery import Celery, chain, states
from fastapi import FastAPI, Depends, HTTPException, status, Body, Query, Path
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from fastapi_versionizer.versionizer import api_version, versionize
from jinja2 import Template
from geopy.distance import great_circle
from plantuml import PlantUML
from starlette.config import Config
from starlette.requests import Request
from starlette.responses import JSONResponse, RedirectResponse, StreamingResponse
from tzlocal import get_localzone

from ska_src_data_management_api import models
from ska_src_data_management_api.auth.oauth2.oauth2 import OAuth2ServiceTokenFactory
from ska_src_data_management_api.cache.redis import RedisCache
from ska_src_data_management_api.common import constants
from ska_src_data_management_api.common.exceptions import handle_exceptions, DIDNotFound, \
    GeolocationDatabaseError, ClientTokenError, DataIngestServiceDisabled, DataIngestServiceNotFound, \
    PermissionDenied, PrepareDataNotFound, SchemaNotFound, StorageAreaNotFound
from ska_src_data_management_api.common.utility import convert_readme_to_html_docs, get_api_server_url_from_request, \
    get_base_url_from_request, get_url_for_app_from_request, retry_request, retry_request_streamed_response
from ska_src_data_management_api.rest import dependencies
from ska_src_data_management_api.session import AuthenticatedRucioOAuth2Session, \
    AuthenticatedSiteCapabilitiesAPIOAuth2Session
from ska_src_data_management_api.tasks.celery import celery
from ska_src_data_management_api.tasks.data import move_data, prepare_data, unpack_move_data_return_for_prepare_data, \
    select_best_staging_sa
from ska_src_permissions_api.client.permissions import PermissionsClient
from ska_src_site_capabilities_api.client.site_capabilities import SiteCapabilitiesClient


config = Config(".env")

# Debug mode (runs unauthenticated)
#
DEBUG = True if config.get("DISABLE_AUTHENTICATION", default=None) == 'yes' else False

# Instantiate FastAPI() allowing CORS. Static mounts must be added later after the versionize() call.
app = FastAPI()
CORSMiddleware_params = {
    "allow_origins": ["*"],
    "allow_credentials": True,
    "allow_methods": ["*"],
    "allow_headers": ["*"]
}
app.add_middleware(CORSMiddleware, **CORSMiddleware_params)

# Set logging to use uvicorn logger.
#
logger = logging.getLogger("uvicorn")

# Get instances of constants.
#
RUCIO_CONSTANTS = constants.Rucio(rucio_host_url=config.get('RUCIO_HOST_URL'))
IAM_CONSTANTS = constants.IAM(client_conf_url=config.get('IAM_CLIENT_CONF_URL'))

# Get templates.
#
TEMPLATES = Jinja2Templates(directory="templates")

# Instantiate token cache if requested.
#
CACHE = None
if config.get('CACHE_TYPE', default='') == 'redis':
    CACHE = RedisCache(host=config.get('CACHE_HOST'), port=config.get('CACHE_PORT'), logger=logger)

# Instantiate geoip2 reader if GEOIP_LICENSE_KEY is set in environment.
#
# This is used to geolocate the nearest replica for a given Rucio data identifier.
#
if config.get("GEOIP_LICENSE_KEY", default=None):
    GEOIP_READER = geoip2.database.Reader(config.get('GEOIP_DATABASE_PATH'))

# Instantiate a Permissions client.
#
PERMISSIONS = PermissionsClient(config.get('PERMISSIONS_API_URL'))
PERMISSIONS_SERVICE_NAME = config.get('PERMISSIONS_SERVICE_NAME')
PERMISSIONS_SERVICE_VERSION = config.get('PERMISSIONS_SERVICE_VERSION')

# Instantiate a service token factory (OAuth2).
#
SERVICE_TOKEN_FACTORY = OAuth2ServiceTokenFactory(
    rucio_admin_client_params={
        'client_id': config.get("RUCIO_ADMIN_CLIENT_ID"),
        'client_secret': config.get("RUCIO_ADMIN_CLIENT_SECRET"),
        'client_scopes': config.get("RUCIO_ADMIN_CLIENT_SCOPES"),
        'client_audience': config.get("RUCIO_ADMIN_CLIENT_AUDIENCE"),
        'iam_token_endpoint': IAM_CONSTANTS.iam_endpoint_token
    },
    site_capabilities_client_params={
        'client_id': config.get("SITE_CAPABILITIES_CLIENT_ID"),
        'client_secret': config.get("SITE_CAPABILITIES_CLIENT_SECRET"),
        'client_scopes': config.get("SITE_CAPABILITIES_CLIENT_SCOPES"),
        'client_audience': config.get("SITE_CAPABILITIES_CLIENT_AUDIENCE"),
        'iam_token_endpoint': IAM_CONSTANTS.iam_endpoint_token
    },
    cache=CACHE,
    logger=logger
)

# Instantiate both permissions based dependencies and a token factory for service token dependencies.
#
permission_dependencies = dependencies.Permissions(
    permissions=PERMISSIONS,
    permissions_service_name=PERMISSIONS_SERVICE_NAME,
    permissions_service_version=PERMISSIONS_SERVICE_VERSION
)
service_token_dependencies = dependencies.ServiceTokens

# Get schemas.
#
# Metadata.
#
metadata_schema_definition_abspath = os.path.join(config.get("SCHEMAS_RELPATH"), config.get("METADATA_SCHEMA_NAME"))
with open(metadata_schema_definition_abspath) as f:
    try:
        METADATA_SCHEMA = json.loads(f.read())
    except jsonschema.exceptions.SchemaError as e:
        logger.error(repr(e))

# Store service start time.
#
SERVICE_START_TIME = time.time()

# Keep track of number of managed requests.
#
REQUESTS_COUNTER = 0
REQUESTS_COUNTER_LOCK = asyncio.Lock()


@handle_exceptions
async def increment_request_counter(request: Request) -> Union[dict, HTTPException]:
    """ Dependency to keep track of API requests. """
    global REQUESTS_COUNTER
    async with REQUESTS_COUNTER_LOCK:
        REQUESTS_COUNTER += 1


# Routes
# ------
#
@api_version(1)
@app.get("/schemas",
         responses={
             200: {"model": models.response.SchemasResponse},
             401: {},
             403: {},
         },
         dependencies=[Depends(increment_request_counter)] if DEBUG else [
             Depends(increment_request_counter),
             Depends(permission_dependencies.verify_permission_for_service_route)
         ],
         tags=["Schemas"],
         summary="List schemas")
async def list_schemas(request: Request) -> JSONResponse:
    """ List schemas. """
    schema_basenames = sorted([''.join(fi.split('.')[:-1]) for fi in os.listdir(config.get('SCHEMAS_RELPATH'))])
    return JSONResponse(schema_basenames)


@api_version(1)
@app.get("/schemas/{schema}",
         responses={
             200: {"model": models.response.SchemaGetResponse},
             401: {},
             403: {},
             404: {"model": models.response.GenericErrorResponse}
         },
         dependencies=[Depends(increment_request_counter)] if DEBUG else [
             Depends(increment_request_counter),
             Depends(permission_dependencies.verify_permission_for_service_route)
         ],
         tags=["Schemas"],
         summary="Get schema")
async def get_schema(request: Request,
                     schema: str = Path(description="Schema name")) \
        -> Union[JSONResponse, HTTPException]:
    """ Get a schema by name. """
    try:
        schema_path = pathlib.Path("{}.json".format(os.path.join(config.get('SCHEMAS_RELPATH'), schema))).absolute()
        with open(schema_path) as f:
            dereferenced_schema = jsonref.load(f, base_uri=schema_path.as_uri())
        return JSONResponse(ast.literal_eval(str(dereferenced_schema)))
    except FileNotFoundError:
        raise HTTPException(status.HTTP_404_NOT_FOUND, "Schema with name '{}' not found".format(schema))


@api_version(1)
@app.get("/schemas/render/{schema}",
         responses={
             200: {},
             401: {},
             403: {},
             404: {"model": models.response.GenericErrorResponse}
         },
         dependencies=[Depends(increment_request_counter)] if DEBUG else [
             Depends(increment_request_counter)
         ],
         tags=["Schemas"],
         summary="Render a schema")
@handle_exceptions
async def render_schema(request: Request,
                     schema: str = Path(description="Schema name")) \
        -> Union[JSONResponse, HTTPException]:
    """ Render a schema by name. """
    try:
        schema_path = pathlib.Path("{}.json".format(os.path.join(config.get('SCHEMAS_RELPATH'), schema))).absolute()
        with open(schema_path) as f:
            dereferenced_schema = ast.literal_eval(str(jsonref.load(f, base_uri=schema_path.as_uri())))
    except FileNotFoundError:
        raise SchemaNotFound

    # Pop countries enum for readability.
    dereferenced_schema.get('properties').get('country', {}).pop('enum', None)

    plantuml = PlantUML(url="http://www.plantuml.com/plantuml/img/")
    with tempfile.NamedTemporaryFile(mode='w', delete=False) as schema_file:
        schema_file.write("@startjson\n{}\n@endjson\n".format(json.dumps(dereferenced_schema, indent=2)))
        schema_file.flush()

        image_temp_file_descriptor, image_temp_file_name = tempfile.mkstemp()
        plantuml.processes_file(filename=schema_file.name, outfile=image_temp_file_name)
        with open(image_temp_file_name, 'rb') as image_temp_file:
            png = image_temp_file.read()
        os.close(image_temp_file_descriptor)

    return StreamingResponse(io.BytesIO(png), media_type="image/png")


@api_version(1)
@app.get('/data/download/{storage_area_uuid}/{namespace}/{name}',
         responses={
             200: {"model": models.response.DataDownloadResponse},
             400: {"model": models.response.GenericErrorResponse},
             401: {},
             403: {}
         },
         dependencies=[Depends(increment_request_counter)] if DEBUG else [
             Depends(increment_request_counter),
             Depends(permission_dependencies.verify_permission_for_service_route)
         ],
         tags=["Data Access"],
         summary="Get a token for download of data")
@handle_exceptions
async def get_download_token(request: Request,
                             storage_area_uuid: str = Path(description="The UUID of the storage area"),
                             namespace: str = Path(description="The namespace (Rucio: DID scope)"),
                             name: str = Path(description="The name (Rucio: DID name)")) \
        -> Union[RedirectResponse, HTTPException]:
    """ Get a file-specific download token for the datalake following the WLCG profile for path authorization.

    More information about the WLCG JWT profile can be found
    [here](https://zenodo.org/record/3460258/files/WLCG_Common_JWT_Profiles_1.0.pdf?download=1).
    """
    # Get storage area information from the external site-capabilities API.
    #
    # For Rucio, this is obtained by looking at storage-areas (RSEs are not services, more blocks of storage).
    #
    site_capabilities = SiteCapabilitiesClient(
        config.get('SITE_CAPABILITIES_URL'),
        session=(await AuthenticatedSiteCapabilitiesAPIOAuth2Session(
            service_token_factory=SERVICE_TOKEN_FACTORY).get()))
    requested_storage_area = site_capabilities.get_storage_area(storage_area_id=storage_area_uuid).json()
    requested_storage_area_relpath = requested_storage_area.get('relative_path')
    requested_storage_area_identifier = requested_storage_area.get('identifier')

    # Get the replica location for this storage area identifier.
    #
    method, url, headers = RUCIO_CONSTANTS.rucio_endpoint_list_replicas
    data = json.dumps({
        'dids': [{
            'scope': namespace,
            'name': name
        }],
        'rse_expression': requested_storage_area_identifier
    }).encode('utf-8')
    response = retry_request(method, url, headers, data=data, session=(await AuthenticatedRucioOAuth2Session(
        service_token_factory=SERVICE_TOKEN_FACTORY).get()))
    if not response.text:
        raise DIDNotFound(namespace, name)

    # Response is formatted as { rse_1: [url_1, url_2, ..., url_n], rse_2: ...}
    replica_at_rse = json.loads(response.text)['rses'].get(requested_storage_area_identifier)[0]

    # Get relative path on storage for token.
    storage_read_path = os.path.join(requested_storage_area_relpath,
                                     replica_at_rse.split(requested_storage_area_relpath)[1].lstrip('/'))
    storage_read_scope = "storage.read:{}".format(storage_read_path)

    access_token_scoped = await SERVICE_TOKEN_FACTORY.get_rucio_service_token(
        audience="https://wlcg.cern.ch/jwt/v1/any",
        additional_scopes=storage_read_scope)
    return JSONResponse({'access_token': access_token_scoped})


@api_version(1)
@app.get('/data/list',
         responses={
             200: {"model": models.response.DataListNamespacesResponse},
             401: {},
             403: {},
             404: {"model": models.response.GenericRucioErrorResponse},
             406: {"model": models.response.GenericRucioErrorResponse}
         },
         dependencies=[Depends(increment_request_counter)] if DEBUG else [
             Depends(increment_request_counter),
             Depends(permission_dependencies.verify_permission_for_service_route)
         ],
         tags=["Data Discovery"],
         summary="List namespaces")
@handle_exceptions
async def list_namespaces(request: Request) \
        -> Union[JSONResponse, HTTPException]:
    """ List namespaces. """
    # Prepare list scopes request and send.
    #
    method, url, headers = RUCIO_CONSTANTS.rucio_endpoint_list_scopes
    response = retry_request(method, url, headers, session=(await AuthenticatedRucioOAuth2Session(
        service_token_factory=SERVICE_TOKEN_FACTORY).get()))

    return JSONResponse(response.json())


@api_version(1)
@app.get('/data/list/{namespace}',
         responses={
             200: {"model": models.response.DataListIdentifiersResponse},
             401: {},
             403: {},
             404: {"model": Union[
                 models.response.GenericErrorResponse,
                 models.response.GenericRucioErrorResponse
             ]},
             406: {"model": models.response.GenericRucioErrorResponse},
             409: {"model": models.response.GenericRucioErrorResponse}
         },
         dependencies=[Depends(increment_request_counter)] if DEBUG else [
             Depends(increment_request_counter),
             Depends(permission_dependencies.verify_permission_for_service_route)
         ],
         tags=["Data Discovery"],
         summary="List data identifiers in namespace")
@handle_exceptions
async def list_identifiers_in_namespace(request: Request,
                                        namespace: str = Path(description="The namespace (Rucio: DID scope)"),
                                        name: str = Query(example='*', description="The name (Rucio: DID name)"),
                                        filters: str = Query(None,
                                                             description="Filter the results by some criteria"),
                                        limit: int = Query(100, description="Limit the number of results")) \
        -> Union[JSONResponse, HTTPException]:
    """ List data identifiers in a namespace, optionally according to some criteria <filter>.

    ### A note on filters (Rucio)

    See [here](https://github.com/rucio/rucio/blob/master/lib/rucio/core/did_meta_plugins/filter_engine.py#L175) for
    how to format the filters expression.

    #### Examples

    1. For bytes less than 10: ```[{"bytes.lte": 10}]```
    2. For bytes between 10 and 100: ```[{"bytes.gte": 10, "bytes.lte": 100}]```
    3. For bytes between 10 and 100 or greater than 1000000: ```[{"bytes.gte": 10, "bytes.lte": 100},
        "bytes.gte": 1000000}]```
    """
    # Prepare list DIDs request and send.
    #
    method, url, headers = RUCIO_CONSTANTS.rucio_endpoint_list_dids
    url = url.format(scope=namespace)
    params = {
        'type': 'all',
        'long': True,
        'limit': limit,
        'filters': filters,
        'name': name
    }
    response = retry_request(method, url, headers, params=params, session=(await AuthenticatedRucioOAuth2Session(
        service_token_factory=SERVICE_TOKEN_FACTORY).get()))

    all_dids = []
    for line in response.iter_lines():
        all_dids.append(json.loads(line))
    if not all_dids:
        raise DIDNotFound(namespace, name)
    #return StreamingResponse(response.iter_lines(), media_type='application/json')
    return JSONResponse(all_dids)


@api_version(1)
@app.get('/data/locate/{namespace}/{name}',
         responses={
             200: {"model": Union[
                 models.response.DataLocateReplicasResponse,
                 models.response.DataLocateReplicasWithServicesResponse
             ]},
             400: {"model": models.response.GenericRucioErrorResponse},
             401: {},
             403: {},
             404: {"model": Union[
                 models.response.GenericErrorResponse,
                 models.response.GenericRucioErrorResponse
             ]},
             406: {"model": models.response.GenericRucioErrorResponse},
             409: {"model": models.response.GenericRucioErrorResponse}
         },
         dependencies=[Depends(increment_request_counter)] if DEBUG else [
             Depends(increment_request_counter),
             Depends(permission_dependencies.verify_permission_for_service_route)
         ],
         tags=["Data Location"],
         summary="Locate replicas of a file")
@handle_exceptions
async def locate_replicas_of_file(request: Request,
                                  namespace: str = Path(description="The namespace (Rucio: DID scope)"),
                                  name: str = Path(example=None, description="The name (Rucio: DID name)"),
                                  ip_address: Union[str, None] = Query(None,
                                                                       description="The ip address to geolocate the "
                                                                                   "nearest replica to. Leave blank to "
                                                                                   "use the requesting client ip "
                                                                                   "(sort == nearest_by_ip only)"),
                                  sort: str = Query('random', examples=["random", "nearest_by_ip"],
                                                    description="The sorting algorithm to use "
                                                                "(random || nearest_by_ip)"),
                                  limit: int = Query(10, description="Limit the number of results"),
                                  colocated_services: str = Query(None, description="Comma separated list of "
                                                                                           "colocated services")) \
        -> Union[JSONResponse, HTTPException]:
    """ Locate the replicas of a (Rucio) data identifier of type FILE, optionally unioned with colocated services.

    See https://site-capabilities.srcdev.skao.int/api/v1/schemas/compute-service for an enumerated list of service
    types.
    """
    # Prepare list replicas request and send.
    #
    method, url, headers = RUCIO_CONSTANTS.rucio_endpoint_list_replicas
    data = json.dumps({
        'dids': [{
            'scope': namespace,
            'name': name
        }]
    }).encode('utf-8')
    response = retry_request(method, url, headers, data=data, session=(await AuthenticatedRucioOAuth2Session(
        service_token_factory=SERVICE_TOKEN_FACTORY).get()))
    if not response.text:
        raise DIDNotFound(namespace, name)

    # Response is formatted as { rse_1: [url_1, url_2, ..., url_n], rse_2: ...}
    replicas_by_rse = json.loads(response.text)['rses']

    # Get a list of storage areas from the external site-capabilities API.
    #
    # For Rucio, this is obtained by looking at storage-areas (RSEs are not services, more blocks of storage).
    #
    site_capabilities = SiteCapabilitiesClient(
        config.get('SITE_CAPABILITIES_URL'),
        session=(await AuthenticatedSiteCapabilitiesAPIOAuth2Session(
            service_token_factory=SERVICE_TOKEN_FACTORY).get()))
    storages_list = site_capabilities.list_storages().json()

    rses_by_location = {}
    storage_area_uuids_by_rse = {}
    for entry in storages_list:
        for storage in entry.get('storages', []):
            for storage_area in storage.get('areas', []):
                if storage_area.get('type') == 'rse':
                    rses_by_location[storage_area.get('identifier')] = \
                        (storage.get('latitude'), storage.get('longitude'))
                    storage_area_uuids_by_rse[storage_area.get('identifier')] = storage_area.get('id')

    # Sort replica order according to the sort algorithm.
    #
    # Output format is:
    # {
    #   site: [replica_location_1, replica_location_2, ...]
    # }
    #
    if sort == 'random':
        # For "random", get a list of all the replica locations and shuffle them
        # randomly.
        #
        sorted_replicas_by_rse = OrderedDict({key: value for key, value in sorted(
            replicas_by_rse.items(), key=lambda k: random.random())})
    elif sort == 'nearest_by_ip':
        # For "nearest_by_ip" we get the nearest storage site to the geolocated ip
        # address.
        #
        # First, we geolocate the ip_address using geoip2.
        #
        if not GEOIP_READER:
            raise GeolocationDatabaseError()

        if not ip_address:   # if not supplied, use the requesting client's
            # ip_address = request.client.host
            # IF PROXIED: requires use-forwarded-headers: 'true' in nginx-ingress cmap
            ip_address = request.headers.get('x-real-ip', '')
        response = GEOIP_READER.city(ip_address)
        location_by_ip = (
            response.location.latitude,
            response.location.longitude
        )

        # Then we order the sites by distance from the requested ip_address.
        #
        distances_by_rse = OrderedDict()
        for rse_identifier, rse_location in rses_by_location.items():
            if rse_identifier in replicas_by_rse:
                distances_by_rse[rse_identifier] = int(
                    great_circle(location_by_ip, rse_location).km)
        sorted_rses = sorted(distances_by_rse, key=distances_by_rse.get)

        sorted_replicas_by_rse = OrderedDict()
        for rse in sorted_rses:
            sorted_replicas_by_rse[rse] = replicas_by_rse[rse]

    # If colocated services are requested, union replica locations with the locations of these services.
    #
    if colocated_services:
        sites = site_capabilities.list_sites_latest().json()

        # Get a list of services by RSE ({'<rse>' -> [services]})
        #
        services_by_rse = {rse: [] for rse in list(sorted_replicas_by_rse.keys())}
        for site in sites:
            # First, get a dictionary of storage areas for this site (id -> storage area)
            storage_areas = {}
            for storage in site.get('storages', []):
                for storage_area in storage.get('areas', []):
                    storage_area_id = storage_area.pop('id')
                    storage_areas[storage_area_id] = storage_area

            # Then iterate through compute and add associated services (with a populated associated_storage_area_id)
            for compute in site.get('compute', []):
                for associated_local_service in compute.get('associated_local_services', []):
                    # Skip disabled services
                    if not associated_local_service.get('enabled', False):
                        continue
                    # Is there any associated storage with this service?
                    associated_storage_area_identifier = associated_local_service.get('associated_storage_area_id')
                    if associated_storage_area_identifier:
                        # Get the corresponding storage area
                        associated_storage_area = storage_areas.get(associated_local_service.get(
                            'associated_storage_area_id'), {})

                        # If the storage-area identifier (Rucio: RSE) is in the list of replica locations
                        #
                        # Note: this effectively means that services that are associated with a storage-area that is
                        # not controlled by Rucio will be excluded.
                        #
                        if associated_storage_area.get('identifier') in services_by_rse.keys():
                            services_by_rse[associated_storage_area.get('identifier')].append(associated_local_service)

        # Union replicas with site service availability.
        #
        colocated_services_list = [service.strip() for service in colocated_services.split(',')]
        requested_colocated_services_by_rse = {}
        for rse, services in services_by_rse.items():
            if set(colocated_services_list).issubset(set([service.get('type') for service in services])):
                requested_colocated_services_by_rse[rse] = services

        # Remove sites where not all the requested services are available.
        #
        for rse in set(sorted_replicas_by_rse.keys()) - set(requested_colocated_services_by_rse.keys()):
            sorted_replicas_by_rse.pop(rse)

    # Handle event that no replicas are found.
    #
    if not sorted_replicas_by_rse:
        if colocated_services:
            details = "No replicas with requested and enabled colocated services found for this DID."
        else:
            details = "No replicas found for this DID."
        raise HTTPException(status.HTTP_404_NOT_FOUND, details)

    # Apply limit (if requested).
    #
    if limit:
        sorted_replicas_by_rse = {key: value for key, value in list(sorted_replicas_by_rse.items())[0:limit]}

    # Construct return format.
    #
    sorted_replicas_by_rse_for_return = [{
        'identifier': rse,
        'associated_storage_area_id': storage_area_uuids_by_rse.get(rse),
        'replicas': replicas,
    } for rse, replicas in list(sorted_replicas_by_rse.items()) if storage_area_uuids_by_rse.get(rse) is not None]

    # Add services (if requested).
    #
    if colocated_services:
        for entry in sorted_replicas_by_rse_for_return:
            rse = entry.get('identifier')
            entry['colocated_services'] = requested_colocated_services_by_rse.get(rse)

    return JSONResponse(sorted_replicas_by_rse_for_return)


@app.post('/data/move',
         responses={
             201: {"model": models.response.DataMoveResponse},
             401: {},
             403: {},
             404: {"model": models.response.GenericErrorResponse},
             406: {"model": models.response.GenericRucioErrorResponse},
             409: {"model": models.response.GenericRucioErrorResponse}
         },
         dependencies=[Depends(increment_request_counter)] if DEBUG else [
             Depends(increment_request_counter),
             Depends(permission_dependencies.verify_permission_for_service_route_data_movement)
         ],
         status_code=201,
         tags=["Data Logistics"],
         summary="Request movement of existing data to another storage area")
@handle_exceptions
async def move_data_to_storage_area(request: Request,
                                    to_storage_area_uuid: Optional[str] = Query(
                                        example='dcbae756-21a9-4787-bff2-2af3b7bfa1b0',
                                        description="The destination storage area uuid"),
                                    lifetime: int = Query(example=3600,
                                                          description="The lifetime of the data (in seconds)"),
                                    parent_namespace: Optional[Union[str, None]] = Query(
                                        default=None,
                                        example=None,
                                        description="(Rucio only) The parent container namespace. Defaults to using "
                                                    "the first DID's namespace."),
                                    dids: list = Body(example=[
                                         {'namespace': 'testing',
                                          'name': 'PTF10tce.fits'
                                          }
                                     ], description="Array of dids to move.")) \
        -> Union[JSONResponse, HTTPException]:
    """ Request movement of existing data to another storage area by addition of replication rules to a set of data
    identifiers if required. """
    # Get the storage area identifier from the uuid.
    #
    # For Rucio, this is obtained by looking at storage-areas (RSEs are not services, more blocks of storage).
    #
    site_capabilities = SiteCapabilitiesClient(
        config.get('SITE_CAPABILITIES_URL'),
        session=(await AuthenticatedSiteCapabilitiesAPIOAuth2Session(
            service_token_factory=SERVICE_TOKEN_FACTORY).get()))
    try:
        storage_area_identifier = site_capabilities.get_storage_area(
            storage_area_id=to_storage_area_uuid).json().get('identifier')
    except HTTPException:
        raise StorageAreaNotFound(to_storage_area_uuid)

    # Call the move_data task and return the job id.
    #
    task = move_data.delay(storage_area_identifier=storage_area_identifier,
                           parent_namespace=parent_namespace,
                           lifetime=lifetime,
                           dids=dids,
                           rucio_host_url=config.get('RUCIO_HOST_URL'),
                           rucio_account=os.environ.get('RUCIO_ACCOUNT', ''),
                           rucio_admin_client_id=config.get("RUCIO_ADMIN_CLIENT_ID"),
                           rucio_admin_client_secret=config.get("RUCIO_ADMIN_CLIENT_SECRET"),
                           rucio_admin_client_scopes=config.get("RUCIO_ADMIN_CLIENT_SCOPES"),
                           rucio_admin_client_audience=config.get("RUCIO_ADMIN_CLIENT_AUDIENCE"),
                           iam_token_endpoint=IAM_CONSTANTS.iam_endpoint_token,
                           cache_type=config.get('CACHE_TYPE', default=''),
                           cache_host=config.get('CACHE_HOST'),
                           cache_port=config.get('CACHE_PORT'),
                           poll_rule_loop_for_total_s=86400,
                           poll_rule_loop_delay_s=10
                           )
    return JSONResponse({'job_id': task.id}, status_code=status.HTTP_201_CREATED)


@api_version(1)
@app.delete('/data/remove',
         responses={
             200: {"model": models.response.DataRemoveResponse},
             401: {},
             403: {},
             404: {"model": models.response.GenericErrorResponse},
             406: {"model": models.response.GenericRucioErrorResponse},
             409: {"model": models.response.GenericRucioErrorResponse}
         },
         dependencies=[Depends(increment_request_counter)] if DEBUG else [
             Depends(increment_request_counter),
             Depends(permission_dependencies.verify_permission_for_service_route)
         ],
         tags=["Data Logistics"],
         summary="Remove data from a storage area")
@handle_exceptions
async def remove_data_from_storage_area(request: Request,
                                        from_storage_area_uuid: str = Query(
                                            description="The storage area identifier or expression to remove the data "
                                                        "from"),
                                        dids: list = Body(example=[{'namespace': 'testing', 'name': 'PTF10tce.fits'}],
                                            description="Array of DIDs to remove for")) \
        -> Union[JSONResponse, HTTPException]:
    """ Request removal of existing data to from a storage area by removal of replication rules from a set of data
    identifiers. """
    # Get the storage area identifier from the uuid.
    #
    # For Rucio, this is obtained by looking at storage-areas (RSEs are not services, more blocks of storage).
    #
    site_capabilities = SiteCapabilitiesClient(
        config.get('SITE_CAPABILITIES_URL'),
        session=(await AuthenticatedSiteCapabilitiesAPIOAuth2Session(
            service_token_factory=SERVICE_TOKEN_FACTORY).get()))
    try:
        storage_area_identifier = site_capabilities.get_storage_area(
            storage_area_id=from_storage_area_uuid).json().get('identifier')
    except HTTPException:
        raise StorageAreaNotFound(from_storage_area_uuid)


    # Instantiate an authenticated Rucio session.
    #
    rucio_session = AuthenticatedRucioOAuth2Session(service_token_factory=SERVICE_TOKEN_FACTORY)

    # Get the list of rule IDs matching the criteria for RSE and account for each requested DID.
    #
    rule_ids_to_delete = []
    for did in dids:
        namespace = did.get('namespace')
        name = did.get('name')

        # Prepare list rules request and send.
        #
        method, url, headers = RUCIO_CONSTANTS.rucio_endpoint_list_rules
        url = url.format(scope=namespace, name=name)
        response = retry_request_streamed(method, url, headers, session=(await rucio_session.get()))

        for line in response.iter_lines():
            line_json = json.loads(line)
            if line_json.get('rse_expression') == storage_area_identifier and \
                    line_json.get('account') == os.environ.get('RUCIO_ACCOUNT', ''):
                rule_ids_to_delete.append(line_json.get('id'))

    # Delete these rule IDs.
    #
    for rule_id in rule_ids_to_delete:
        method, url, headers  = RUCIO_CONSTANTS.rucio_endpoint_delete_rule
        url = url.format(rule_id=rule_id)
        data = json.dumps({}).encode('utf-8')
        retry_request(method, url, headers, data=data, session=(await rucio_session.get()))

    return JSONResponse({"acknowledged": True}, status_code=201)


@app.get('/data/move/{job_id}',
         responses={
             200: {"model": models.response.DataMoveStatusResponse},
             401: {},
             403: {},
             404: {"model": models.response.GenericErrorResponse},
             406: {"model": models.response.GenericRucioErrorResponse},
             409: {"model": models.response.GenericRucioErrorResponse}
         },
         dependencies=[Depends(increment_request_counter)] if DEBUG else [
             Depends(increment_request_counter),
             Depends(permission_dependencies.verify_permission_for_service_route)
         ],
         tags=["Data Logistics"],
         summary="Get the status of a data movement request")
@handle_exceptions
async def get_data_movement_status(request: Request,
                                   job_id: str = Path(example='84a355e6-856f-4426-8eec-9001f6165042',
                                                      description="The data movement job id")) \
        -> Union[JSONResponse, HTTPException]:
    """ Get the status of a data movement request. """
    task = celery.AsyncResult(job_id)

    if not task.ready():
        return JSONResponse({
            'status': task.state,
            'info': task.info
        })
    else:
        return JSONResponse(task.get())


@app.post('/data/stage',
         responses={
             201: {"model": models.response.DataStageResponse},
             401: {},
             403: {},
             404: {"model": models.response.GenericErrorResponse},
             406: {"model": models.response.GenericRucioErrorResponse},
             409: {"model": models.response.GenericRucioErrorResponse}
         },
         dependencies=[Depends(increment_request_counter)] if DEBUG else [
             Depends(increment_request_counter),
             Depends(permission_dependencies.verify_permission_for_service_route_data_movement)
         ],
         status_code=201,
         tags=["Data Staging"],
         summary="Request staging of existing data in a storage area")
@handle_exceptions
async def stage_data_in_storage_area(request: Request,
                                     to_storage_area_uuid: Optional[str] = Query(
                                         default=None,
                                         example='dcbae756-21a9-4787-bff2-2af3b7bfa1b0',
                                         description="The destination storage area uuid"),
                                     lifetime: int = Query(example=3600,
                                                           description="The lifetime of the data (in seconds)"),
                                     parent_namespace: Optional[Union[str, None]] = Query(
                                         default=None,
                                         example="testing",
                                         description="(Rucio only) The parent container namespace. Defaults to using "
                                                     "the first DID's namespace."),
                                     dids: list = Body(example=[
                                         {'namespace': 'testing',
                                          'name': 'PTF10tce.fits'}
                                     ], description="Array of files to replicate.")) \
        -> Union[JSONResponse, HTTPException]:
    """ Request staging of existing data in a storage area. """
    # Get the storage area identifier from the uuid.
    #
    # For Rucio, this is obtained by looking at storage-areas (RSEs are not services, more blocks of storage).
    #
    site_capabilities = SiteCapabilitiesClient(
        config.get('SITE_CAPABILITIES_URL'),
        session=(await AuthenticatedSiteCapabilitiesAPIOAuth2Session(
            service_token_factory=SERVICE_TOKEN_FACTORY).get()))
    
    if to_storage_area_uuid:
        try:
            storage_area_identifier = site_capabilities.get_storage_area(
                storage_area_id=to_storage_area_uuid).json().get('identifier')
        except HTTPException:
            raise StorageAreaNotFound(to_storage_area_uuid)
    else:
        storage_area_identifier=None
    
    # Build a {storage_area_identifier: prepareData URL} map
    # Get all sites, storages and services
    # TODO: Add Site Cap client methods to streamline this; sites could have multiple PD svcs
    #
    site_ids = site_capabilities.list_sites().json()
    all_storages = site_capabilities.list_storages().json()
    all_services = site_capabilities.list_services().json()

    storage_area_props = {}
    for site in site_ids:
        logger.info("Identifying prepare_data services for site {}".format(site))
        site_storages = next((sto for sto in all_storages if sto["site_name"] == site), None)
        site_services = next((svc for svc in all_services if svc["site_name"] == site), None)
        
        # Find site preparedata URL
        #
        preparedata = next(
            (pd for pd in site_services["services"] if pd["type"] == "prepare_data"), None
        )
        if preparedata:
            try:
                pd_url = "{}://{}:{}".format(
                    preparedata['prefix'],
                    preparedata['host'],
                    preparedata['port']
                )
                if preparedata.get('path'):
                    pd_url = "{}/{}".format(pd_url, preparedata['path'].lstrip("/"))
            except KeyError as e:
                logger.warning(
                    "Error parsing prepareData service for site {}: {}".format(site, e)
                )
                continue
        else:
            logger.warning(
                "No prepareData service found for site {}".format(site)
            )
            continue

        # Map site storage areas to this preparedata service URL
        #
        for storage in site_storages["storages"]:
            for area in storage.get("areas", []):
                if "identifier" in area and "relative_path" in area: # TODO: and area.get("tier") == "0":
                    sa_prop = {
                        "preparedata_url": pd_url,
                        "storage_root": os.path.join(
                            storage["base_path"],
                            area["relative_path"].lstrip("/")
                        )
                    }
                    storage_area_props[area["identifier"]] = sa_prop
                else:
                    logger.warning("Warning: Area {} is missing an identifier or relative_path.".format(area['id']))

    if not storage_area_props:
        logger.critical("No suitable prepare data services found")
        raise PrepareDataNotFound()
    
    # Set the relative paths DIDs should be staged at within the user's storage:
    # Fo now, just set to the relative path to be equal to the task ID
    #
    did_to_relpath_mappings = [
        {**did, 'relative_path': './{}/'.format(did["namespace"])} for did in dids
    ]

    # Chain the move_data and prepare_data tasks and return the job id.
    #
    # An intermediate task is required to first unpack the dictionary contents of move_data, otherwise the signature for
    # the prepare_data task would need to be the return from move_data, making it dependent on running move_data first
    # (which may not always be the case).
    #
    common_task_kwargs = {
        "rucio_host_url": config.get('RUCIO_HOST_URL'),
        "rucio_admin_client_id": config.get("RUCIO_ADMIN_CLIENT_ID"),
        "rucio_admin_client_secret": config.get("RUCIO_ADMIN_CLIENT_SECRET"),
        "rucio_admin_client_scopes": config.get("RUCIO_ADMIN_CLIENT_SCOPES"),
        "rucio_admin_client_audience": config.get("RUCIO_ADMIN_CLIENT_AUDIENCE"),
        "iam_token_endpoint": IAM_CONSTANTS.iam_endpoint_token,
        "cache_type": config.get('CACHE_TYPE', default=''),
        "cache_host": config.get('CACHE_HOST'),
        "cache_port": config.get('CACHE_PORT')
    }
    task = chain(
        select_best_staging_sa.s(
            storage_area_identifier=storage_area_identifier,
            dids=dids,
            storage_area_props=storage_area_props,
            **common_task_kwargs
        ),
        move_data.s(
            parent_namespace=parent_namespace,
            lifetime=lifetime,
            dids=dids,
            rucio_account=os.environ.get('RUCIO_ACCOUNT', ''),
            poll_rule_loop_for_total_s=86400,
            poll_rule_loop_delay_s=10,
            **common_task_kwargs
        ),
        unpack_move_data_return_for_prepare_data.s(),
        prepare_data.s(
            storage_area_props=storage_area_props,
            preparation_strategy=constants.DataPreparingStrategies.SYMLINKS,
            user_authz_header=request.headers.get('Authorization'),
            did_to_relpath_mappings=did_to_relpath_mappings,
            **common_task_kwargs
        )
    ).apply_async()

    return JSONResponse({'job_id': task.id}, status_code=status.HTTP_201_CREATED)


@app.get('/data/stage/{job_id}',
         responses={
             200: {"model": models.response.DataStageStatusResponse},
             401: {},
             403: {},
             404: {"model": models.response.GenericErrorResponse},
             406: {"model": models.response.GenericRucioErrorResponse},
             409: {"model": models.response.GenericRucioErrorResponse}
         },
         dependencies=[Depends(increment_request_counter)] if DEBUG else [
             Depends(increment_request_counter),
             Depends(permission_dependencies.verify_permission_for_service_route)
         ],
         tags=["Data Staging"],
         summary="Get the status of a data staging request")
@handle_exceptions
async def get_data_staging_status(request: Request,
                                  job_id: str = Path(example='84a355e6-856f-4426-8eec-9001f6165042',
                                                      description="The data staging job id")) \
        -> Union[JSONResponse, HTTPException]:
    """ Get the status of a data staging request. """
    task = celery.AsyncResult(job_id)

    # Store the task state in a local variable here, in case it changes while parsing.
    task_state = task.state

    # Return a suitable response based on the task state. The handled states are all
    # states in celery.states.ALL_STATES, and the custom states set in
    # ska_src_data_management_api.tasks.data.prepare_data().
    in_progress_states = [states.PENDING, states.STARTED, states.RETRY, states.RECEIVED,
                          constants.DataPreparingStatus.VERIFYING,
                          constants.DataPreparingStatus.PREPARING]
    if task_state in in_progress_states:
        return JSONResponse(
            status_code=200,
            content={'status': task_state, 'info': task.info or "Task is still in progress"}
        )
    elif task_state == constants.DataPreparingStatus.ERROR:
        return JSONResponse(
            status_code=200,
            content={'status': task_state, 'result': task.result}
        )
    elif task_state == states.REVOKED:
        return JSONResponse(
            status_code=200,
            content={'status': task_state, 'info': task.info or "Task was revoked."}
        )
    elif task_state == states.FAILURE:
        return JSONResponse(
            status_code=200,
            content={
                'status': 'FAILED',
                'error_message': str(task.result),
                'traceback': task.traceback
            }
        )
    elif task_state == states.SUCCESS:
        return JSONResponse(
            status_code=200,
            content={'status': 'COMPLETED', 'result': task.result}
        )
    return JSONResponse(
        status_code=500,
        content={'status': 'UNKNOWN', 'message': 'Unexpected task state'}
    )


@api_version(1)
@app.get('/data/upload/ingest/{data_ingest_service_uuid}/{namespace}',
         responses={
             200: {"model": models.response.DataUploadIngestResponse},
             400: {"model": models.response.GenericErrorResponse},
             401: {},
             403: {}
         },
         dependencies=[Depends(increment_request_counter)] if DEBUG else [
             Depends(increment_request_counter),
             Depends(permission_dependencies.verify_permission_for_service_route)
         ],
         tags=["Data Access"],
         summary="Get a token for upload of data for ingest")
@handle_exceptions
async def get_upload_token_ingest(request: Request,
                                  data_ingest_service_uuid: str = Path(description="The UUID of the data ingest "
                                                                                    "service to be used"),
                                  namespace: str = Path(description="The namespace (Rucio: DID scope)")) \
        -> Union[RedirectResponse, HTTPException]:
    """ Get an upload token to upload data to an ingest area following the WLCG profile for path authorization.

    More information about the WLCG JWT profile can be found
    [here](https://zenodo.org/record/3460258/files/WLCG_Common_JWT_Profiles_1.0.pdf?download=1).
    """
    site_capabilities = SiteCapabilitiesClient(
        config.get('SITE_CAPABILITIES_URL'),
        session=(await AuthenticatedSiteCapabilitiesAPIOAuth2Session(
            service_token_factory=SERVICE_TOKEN_FACTORY).get()))

    # See if there exists a service with the requested data_ingest_service_uuid.
    try:
        data_ingest_service = site_capabilities.get_service(service_id=data_ingest_service_uuid).json()
        if not data_ingest_service.get('enabled', False):
            raise DataIngestServiceDisabled(data_ingest_service_uuid)
    except HTTPException:
        raise DataIngestServiceNotFound(data_ingest_service_uuid)

    # Get the relative access path from the associated storage area to this data ingest service.
    associated_storage_area = site_capabilities.get_storage_area(
        data_ingest_service.get('associated_storage_area_id')).json()
    if not associated_storage_area:
        raise StorageAreaNotFound(data_ingest_service.get('associated_storage_area_id'))

    # Construct the relative staging access path for the token from the storage's base path.
    relative_path_for_token = '/{}/{}'.format(
        associated_storage_area.get('relative_path').lstrip('/').rstrip('/'), namespace)

    # Get a scope limited storage token.
    storage_write_scope = "storage.create:{}".format(relative_path_for_token)
    access_token_scoped = await SERVICE_TOKEN_FACTORY.get_rucio_service_token(
        audience="https://wlcg.cern.ch/jwt/v1/any",
        additional_scopes=storage_write_scope)

    return JSONResponse({'access_token': access_token_scoped})


@api_version(1)
@app.get("/metadata/{namespace}/{name}",
         responses={
             200: {"model": models.response.MetadataGetResponse},
             401: {},
             403: {},
             404: {"model": Union[
                 models.response.GenericErrorResponse,
                 models.response.GenericRucioErrorResponse
             ]},
             406: {"model": models.response.GenericRucioErrorResponse},
         },
         dependencies=[Depends(increment_request_counter)] if DEBUG else [
             Depends(increment_request_counter),
             Depends(permission_dependencies.verify_permission_for_service_route)
         ],
         tags=["Metadata"],
         summary="Get metadata for a data identifier")
@handle_exceptions
async def get_metadata(request: Request,
                       namespace: str = Path(description="The namespace (Rucio: DID scope)"),
                       name: str = Path(description="The names (Rucio: DID name)"),
                       plugin: str = Query(default="POSTGRES_JSON", examples=["POSTGRES_JSON", "DID_COLUMN"],
                                           description="(Rucio only) The metadata plugin to use")) \
        -> Union[JSONResponse, HTTPException]:
    """ Get the metadata belonging to a data identifier.

    ### Plugins (Rucio only)

    Rucio uses the concept of metadata "plugins". For application level metadata use **DID_COLUMN**, for custom
    metadata use **POSTGRES_JSON**.
    """
    # Prepare get metadata request and send.
    #
    method, url, headers = RUCIO_CONSTANTS.rucio_endpoint_get_metadata
    url = url.format(scope_name="{}/{}".format(namespace, name))
    params = {
        "plugin": plugin
        if plugin in ["POSTGRES_JSON", "DID_COLUMN"]
        else "POSTGRES_JSON"
    }
    response = retry_request(method, url, headers, params=params, session=(await AuthenticatedRucioOAuth2Session(
        service_token_factory=SERVICE_TOKEN_FACTORY).get()))

    return JSONResponse(json.loads(response.text))


@api_version(1)
@app.post("/metadata/{namespace}/{name}",
          status_code=201,
          responses={
              201: {"model": models.response.GenericOperationResponse},
              401: {},
              403: {},
              404: {"model": Union[
                  models.response.GenericErrorResponse,
                  models.response.GenericRucioErrorResponse
              ]},
              406: {"model": models.response.GenericRucioErrorResponse},

          },
          dependencies=[Depends(increment_request_counter)] if DEBUG else [
              Depends(increment_request_counter),
              Depends(permission_dependencies.verify_permission_for_service_route)
          ],
          tags=["Metadata"],
          summary="Set metadata for a data identifier")
@handle_exceptions
async def set_metadata(request: Request,
                       namespace: str = Path(description="The namespace (Rucio: DID scope)"),
                       name: str = Path(description="The name (Rucio: DID name)"),
                       metadata: Union[str, dict] = Body(
                           example='{"test_key": "test_value"}', description="The metadata to add")) \
        -> Union[JSONResponse, HTTPException]:
    """ Set the metadata on a data identifier.

    ### Metadata routing (Rucio only)

    Rucio uses the concept of metadata "plugins". For keys that match internal application metadata, the metadata will
    be stored in the internal (**DID_COLUMN**) metadata store. For all other keys, the metadata will be stored in the
    external (**POSTGRES_JSON**) metadata store.

    For a list of internal keys, see
    [here](https://github.com/rucio/rucio/blob/master/lib/rucio/db/sqla/models.py#L372).
    """
    # Validate metadata schema.
    try:
        jsonschema.validate(metadata, METADATA_SCHEMA)
    except jsonschema.exceptions.ValidationError as e:
        raise HTTPException(status.HTTP_400_BAD_REQUEST, "metadata schema validation error: {}".format(repr(e)))

    # Prepare set metadata request and send.
    #
    method, url, headers = RUCIO_CONSTANTS.rucio_endpoint_set_metadata
    url = url.format(scope_name="{}/{}".format(namespace, name))
    if isinstance(metadata, dict):
        metadata = json.dumps(metadata)
    retry_request(method, url, headers, json={'meta': ast.literal_eval(metadata)},
                  session=(await AuthenticatedRucioOAuth2Session(service_token_factory=SERVICE_TOKEN_FACTORY).get()))

    return JSONResponse({"successful": True}, status_code=201)


@api_version(1)
@app.delete("/metadata/{namespace}/{name}",
          status_code=201,
          responses={
              201: {"model": models.response.GenericOperationResponse},
              401: {},
              403: {},
              404: {"model": Union[
                  models.response.GenericErrorResponse,
                  models.response.GenericRucioErrorResponse
              ]},
              406: {"model": models.response.GenericRucioErrorResponse},

          },
          dependencies=[Depends(increment_request_counter)] if DEBUG else [
              Depends(increment_request_counter),
              Depends(permission_dependencies.verify_permission_for_service_route)
          ],
          tags=["Metadata"],
          summary="Delete metadata for a data identifier")
@handle_exceptions
async def set_metadata(request: Request,
                       namespace: str = Path(description="The namespace (Rucio: DID scope)"),
                       name: str = Path(description="The name (Rucio: DID name)"),
                       key: str = Query(description="The metadata key")) \
        -> Union[JSONResponse, HTTPException]:
    """ Delete metadata on a data identifier.

    ### Metadata routing (Rucio only)

    Rucio uses the concept of metadata "plugins". For keys that match internal application metadata, the metadata will
    be stored in the internal (**DID_COLUMN**) metadata store. For all other keys, the metadata will be stored in the
    external (**POSTGRES_JSON**) metadata store.

    For a list of internal keys, see
    [here](https://github.com/rucio/rucio/blob/master/lib/rucio/db/sqla/models.py#L372).
    """
    # Prepare delete metadata request and send.
    #
    method, url, headers = RUCIO_CONSTANTS.rucio_endpoint_delete_metadata
    url = url.format(scope_name="{}/{}".format(namespace, name))
    params = {
        'key': key
    }
    retry_request(method, url, headers=headers, params=params,
                  session=(await AuthenticatedRucioOAuth2Session(service_token_factory=SERVICE_TOKEN_FACTORY).get()))

    return JSONResponse({"successful": True}, status_code=201)


@api_version(1)
@app.get("/www/docs/oper",
         include_in_schema=False,
         dependencies=[Depends(increment_request_counter)] if DEBUG else [
             Depends(increment_request_counter)
         ])
@handle_exceptions
async def oper_docs(request: Request) -> TEMPLATES.TemplateResponse:
    # Read and parse README.md, omitting excluded sections.
    if not DEBUG:
        readme_text_md = os.environ.get('README_MD', "")
    else:
        with open("../../../README.md") as f:
            readme_text_md = f.read()
    readme_text_html = convert_readme_to_html_docs(readme_text_md, exclude_sections=["Deployment"])
    openapi_schema = request.scope.get('app').openapi_schema
    openapi_schema_template = Template(json.dumps(openapi_schema))
    return TEMPLATES.TemplateResponse("docs.html", {
        "request": request,
        "base_url": get_base_url_from_request(request, config.get('API_SCHEME', default='http')),
        "page_title": "Data Management API Operator Documentation",
        "openapi_schema": openapi_schema_template.render({
            "api_server_url": get_api_server_url_from_request(request, config.get('API_SCHEME', default='http'))
        }),
        "readme_text_md": readme_text_html,
        "version": "v{version}".format(version=os.environ.get('SERVICE_VERSION'))
    })


@api_version(1)
@app.get("/www/docs/user",
         include_in_schema=False,
         dependencies=[Depends(increment_request_counter)] if DEBUG else [
             Depends(increment_request_counter)
         ])
@handle_exceptions
async def user_docs(request: Request) -> TEMPLATES.TemplateResponse:
    # Read and parse README.md, omitting excluded sections.
    if not DEBUG:
        readme_text_md = os.environ.get('README_MD', "")
    else:
        with open("../../../README.md") as f:
            readme_text_md = f.read()
    readme_text_html = convert_readme_to_html_docs(readme_text_md, exclude_sections=[
        "Authorisation", "Deployment"])

    # Exclude unnecessary paths.
    paths_to_include = {
        '/data/download/{storage_area_uuid}/{namespace}/{name}': ['get'],
        '/data/list': ['get'],
        '/data/list/{namespace}': ['get'],
        '/data/locate/{namespace}/{name}': ['get'],
        '/data/move/': ['post'],
        '/data/move/{job_id}': ['get'],
        '/data/stage/': ['post'],
        '/data/stage/{job_id}': ['get'],
        '/data/upload/ingest/{data_ingest_service_uuid}/{namespace}': ['get'],
        '/metadata/{namespace}/{name}': ['get', 'post'],
        '/ping': ['get'],
        '/health': ['get']
    }
    openapi_schema = copy.deepcopy(request.scope.get('app').openapi_schema)
    included_paths = {}
    for path, methods in openapi_schema.get('paths', {}).items():
        for method, attr in methods.items():
            if method in paths_to_include.get(path, []):
                if path not in included_paths:
                    included_paths[path] = {}
                included_paths[path][method] = attr
    openapi_schema.update({'paths': included_paths})

    openapi_schema_template = Template(json.dumps(openapi_schema))
    return TEMPLATES.TemplateResponse("docs.html", {
        "request": request,
        "base_url": get_base_url_from_request(request, config.get('API_SCHEME', default='http')),
        "page_title": "Data Management API User Documentation",
        "openapi_schema": openapi_schema_template.render({
            "api_server_url": get_api_server_url_from_request(request, config.get('API_SCHEME', default='http'))
        }),
        "readme_text_md": readme_text_html,
        "version": "v{version}".format(version=os.environ.get('SERVICE_VERSION'))
    })


@api_version(1)
@app.get('/ping',
         responses={
             200: {"model": models.response.PingResponse},
             401: {},
             403: {}
         },
         tags=["Status"],
         summary="Check API status")
@handle_exceptions
async def ping(request: Request):
    """ Service aliveness. """
    return JSONResponse({
        'status': "UP",
        'version': os.environ.get('SERVICE_VERSION'),
    })


@api_version(1)
@app.get('/health',
         responses={
             200: {"model": models.response.HealthResponse},
             401: {},
             403: {},
             500: {"model": models.response.HealthResponse}
         },
         tags=["Status"],
         summary="Check API health")
@handle_exceptions
async def health(request: Request):
    """ Service health.

    This endpoint will return a 500 if any of the dependent services are down.
    """

    # Dependent services.
    #
    # Rucio API
    #
    # Define url and populate Rucio token header with exchanged token.
    method, url, _headers = RUCIO_CONSTANTS.rucio_endpoint_ping
    # Prepare REST request and send.
    req = requests.Request(method, url).prepare()
    rucio_api_response = requests.Session().send(req)

    # Permissions API
    #
    permissions_api_response = PERMISSIONS.ping()

    # Site Capabilities API
    #
    site_capabilities = SiteCapabilitiesClient(config.get('SITE_CAPABILITIES_URL'))
    site_capabilities_api_response = site_capabilities.ping()

    # Set return code dependent on criteria e.g. dependent service statuses
    #
    healthy_criteria = [
        rucio_api_response.status_code == 200,
        permissions_api_response.status_code == 200,
        site_capabilities_api_response.status_code == 200
    ]
    return JSONResponse(
        status_code=status.HTTP_200_OK if all(healthy_criteria) else status.HTTP_500_INTERNAL_SERVER_ERROR,
        content={
            'uptime': round(time.time() - SERVICE_START_TIME),
            'number_of_managed_requests': REQUESTS_COUNTER,
            'dependent_services': {
                'rucio': {
                    'status': "UP" if rucio_api_response.status_code == 200 else "DOWN",
                },
                'permissions-api': {
                    'status': "UP" if permissions_api_response.status_code == 200 else "DOWN",
                },
                'site-capabilities-api': {
                    'status': "UP" if site_capabilities_api_response.status_code == 200 else "DOWN",
                }
            }
        }
    )


# Versionise the API.
#
versions = versionize(
    app=app,
    prefix_format='/v{major}',
    docs_url=None,
    redoc_url=None
)
app.mount("/static", StaticFiles(directory="static"), name="static")

# Customise openapi.json.
#
# - Add schema server, title and tags.
# - Add request code samples to routes.
# - Remove 422 responses.
#
for route in app.routes:
    if isinstance(route.app, FastAPI):              # find any FastAPI subapplications (e.g. /v1/, /v2/, ...)
        subapp = route.app
        subapp_base_path = '{}{}'.format(os.environ.get('API_ROOT_PATH', default=''), route.path)
        subapp.openapi()
        subapp.openapi_schema['servers'] = [{"url": subapp_base_path}]
        subapp.openapi_schema['info']['title'] = 'Data Management API Overview'
        subapp.openapi_schema['tags'] = [
            {"name": "Data Discovery", "description": "Discover data in the datalake.", "x-tag-expanded": False},
            {"name": "Data Location", "description": "Retrieve access points for data in the datalake.",
             "x-tag-expanded": False},
            {"name": "Data Access", "description": "Access data in the datalake.", "x-tag-expanded": False},
            {"name": "Data Logistics", "description": "Manage the placement of data in the datalake.",
             "x-tag-expanded": False},
            {"name": "Data Staging", "description": "Stage data.", "x-tag-expanded": False},
            {"name": "Metadata", "description": "Metadata operations.", "x-tag-expanded": False},
            {"name": "Schemas", "description": "Schema operations.", "x-tag-expanded": False},
            {"name": "Status", "description": "Operations describing the status of the API.", "x-tag-expanded": False},
        ]
        # add request code samples and strip out 422s
        for language in ['shell', 'python', 'go', 'js']:
            for path, methods in subapp.openapi_schema['paths'].items():
                path = path.strip('/')
                for method, attr in methods.items():
                    if attr.get('responses', {}).get('422'):
                        del attr.get('responses')['422']
                    method = method.strip('/')
                    sample_template_filename = "{}-{}-{}.j2".format(
                        language, path, method).replace('/', '-')
                    sample_template_path = os.path.join('request-code-samples', sample_template_filename)
                    if os.path.exists(sample_template_path):
                        with open(sample_template_path, 'r') as f:
                            sample_source_template = f.read()
                        code_samples = attr.get('x-code-samples', [])
                        code_samples.append({
                            'lang': language,
                            'source': str(sample_source_template)            # rendered later in route
                        })
                        attr['x-code-samples'] = code_samples
