import abc
from typing import Union, Optional

import asyncio
from fastapi import Depends, HTTPException
from fastapi.security import HTTPBearer
from starlette.requests import Request

from ska_src_data_management_api.auth.oauth2.oauth2 import OAuth2ServiceTokenFactory
from ska_src_data_management_api.common.exceptions import handle_exceptions, ClientNotDefinedError, ClientTokenError, \
    PermissionDenied



class Common:
    """ A class to encapsulate all common dependencies. """
    def __init__(self):
        # Keep track of number of managed requests.
        #
        self.requests_counter = 0
        self.requests_counter_lock = asyncio.Lock()

    @handle_exceptions
    async def increment_request_counter(self):
        async with self.requests_counter_lock:
            self.requests_counter += 1


class Permissions:
    """ A class to encapsulate all permissions based dependencies. """
    def __init__(self, permissions, permissions_service_name, permissions_service_version):
        self.permissions = permissions
        self.permissions_service_name = permissions_service_name
        self.permissions_service_version = permissions_service_version

    @handle_exceptions
    async def verify_permission_for_service_route(self, request: Request, authorization: str = Depends(HTTPBearer())) \
            -> Union[HTTPException, bool]:
        """ Dependency to verify permission for a service's route using the bearer token from the request's headers.

        This is the default authz route. Parameters for the verification are passed from the request path parameters.
        """
        if authorization.credentials is None:
            raise PermissionDenied
        access_token = authorization.credentials
        rtn = self.permissions.authorise_service_route(service=self.permissions_service_name,
                                                       version=self.permissions_service_version,
                                                       route=request.scope['route'].path, method=request.method,
                                                       token=access_token, body=request.path_params).json()
        if rtn.get('is_authorised', False):
            return
        raise PermissionDenied

    @handle_exceptions
    async def verify_permission_for_service_route_data_movement(self, request: Request,
                                                                authorization: str = Depends(HTTPBearer())) \
            -> HTTPException:
        """ Dependency to verify permission for data movement routes using the bearer token from the request's headers.

        Parameters for the verification are passed from the request body, expecting a list of dictionaries corresponding
        to DIDs with namespace and name attributes e.g.

        [
            {'namespace': "namespace_1", 'name': "name_1"},
            {'namespace': "namespace_1", 'name': "name_2"},
            ...
        ]

        For each DID in this list, permissions will be checked for the corresponding required group.
        """
        if authorization.credentials is None:
            raise PermissionDenied
        access_token = authorization.credentials

        dids = await request.json()
        for did in dids:
            rtn = self.permissions.authorise_service_route(service=self.permissions_service_name,
                                                           version=self.permissions_service_version,
                                                           route=request.scope['route'].path, method=request.method,
                                                           token=access_token, body=did).json()
            if rtn.get('is_authorised', False):
                continue
            else:
                raise PermissionDenied


class ServiceTokens:
    """ A class to encapsulate all service token based dependencies. """
    class BaseService(abc.ABC):
        def __init__(self, token_factory: OAuth2ServiceTokenFactory, audience: Optional[str] = None,
                     try_use_cache: Optional[bool] = True, additional_scopes: Optional[str] = None):
            self.token_factory = token_factory
            self.audience = audience
            self.try_use_cache = try_use_cache
            self.additional_scopes = additional_scopes

    class Rucio(BaseService):
        """ A Rucio service token dependency.

        This dependency is implemented as a callable class so that parameters are not exposed at user interfaces. See
        https://fastapi.tiangolo.com/advanced/advanced-dependencies/ for more information.
        """
        def __init__(self, token_factory: OAuth2ServiceTokenFactory, audience: Optional[str] = None,
                     try_use_cache: Optional[bool] = True, additional_scopes: Optional[str] = None):
            super().__init__(token_factory=token_factory, audience=audience, try_use_cache=try_use_cache,
                             additional_scopes=additional_scopes)

        @handle_exceptions
        async def __call__(self, request: Request) -> Union[dict, HTTPException]:
            return (await self.token_factory.get_rucio_service_token(audience=self.audience,
                                                                    try_use_cache=self.try_use_cache,
                                                                    additional_scopes=self.additional_scopes))

    class SiteCapabilities(BaseService):
        """ A Site Capabilities API service token dependency.

        This dependency is implemented as a callable class so that parameters are not exposed at user interfaces. See
        https://fastapi.tiangolo.com/advanced/advanced-dependencies/ for more information.
        """
        def __init__(self, token_factory: OAuth2ServiceTokenFactory, audience: Optional[str] = None,
                     try_use_cache: Optional[bool] = True, additional_scopes: Optional[str] = None):
            super().__init__(token_factory=token_factory, audience=audience, try_use_cache=try_use_cache,
                             additional_scopes=additional_scopes)

        @handle_exceptions
        async def __call__(self, request: Request) -> Union[dict, HTTPException]:
            return (await self.token_factory.get_site_capabilities_service_token(
                audience=self.audience, try_use_cache=self.try_use_cache, additional_scopes=self.additional_scopes))
