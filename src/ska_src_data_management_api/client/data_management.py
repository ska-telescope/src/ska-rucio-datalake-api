import json
import requests

from ska_src_data_management_api.common.exceptions import handle_client_exceptions


class DataManagementClient:
    def __init__(self, api_url, session=None):
        self.api_url = api_url
        if session:
            self.session = session
        else:
            self.session = requests.Session()

    @handle_client_exceptions
    def get_schema(self, schema: str):
        """ Get a schema.

        :param str schema: The name of the schema.

        :return: A requests response.
        :rtype: requests.models.Response
        """
        get_schema_endpoint = "{api_url}/schemas/{schema}".format(api_url=self.api_url, schema=schema)
        resp = self.session.get(get_schema_endpoint)
        resp.raise_for_status()
        return resp

    @handle_client_exceptions
    def get_download_token_for_namespace_name(self, storage_area_uuid: str, namespace: str, name: str):
        """ Get token to download data from a storage area, restricted to a specific namespace and name.

        :param str storage_area_uuid: The storage area UUID.
        :param str namespace: The data identifier namespace.
        :param str name: The data identifier name.

        :return: A requests response.
        :rtype: requests.models.Response
        """
        get_download_token_endpoint = "{api_url}/data/download/{storage_area_uuid}/{namespace}/{name}".format(
            api_url=self.api_url,
            storage_area_uuid=storage_area_uuid,
            namespace=namespace,
            name=name)

        resp = self.session.get(get_download_token_endpoint)
        resp.raise_for_status()
        return resp

    @handle_client_exceptions
    def get_metadata(self, namespace: str, name: str, plugin: str = "POSTGRES_JSON"):
        """ Get metadata for a data identifier.

        :param str namespace: The data identifier namespace.
        :param str name: The data identifier name.
        :param str plugin: The metadata plugin to use (Rucio only, can be POSTGRES_JSON or DID_COLUMN).

        :return: A requests response.
        :rtype: requests.models.Response
        """
        get_metadata_endpoint = "{api_url}/metadata/{namespace}/{name}".format(api_url=self.api_url,
                                                                               namespace=namespace,
                                                                               name=name)
        params = {
            "plugin": plugin
        }
        resp = self.session.get(get_metadata_endpoint, params=params)
        resp.raise_for_status()
        return resp

    @handle_client_exceptions
    def get_status_data_movement_request(self, job_id: str):
        """ Get the status of a data movement request.

        :param str job_id: The job id.

        :return: The job parameters.
        :rtype: requests.models.Response
        """
        get_status_data_movement_request_endpoint = "{api_url}/data/move/{job_id}".format(api_url=self.api_url,
                                                                                          job_id=job_id)
        resp = self.session.get(get_status_data_movement_request_endpoint)
        resp.raise_for_status()
        return resp

    @handle_client_exceptions
    def get_status_data_staging_request(self, job_id: str):
        """ Get the status of a data staging request.

        :param str job_id: The job id.

        :return: The job parameters.
        :rtype: requests.models.Response
        """
        get_status_data_staging_request_endpoint = "{api_url}/data/staging/{job_id}".format(api_url=self.api_url,
                                                                                            job_id=job_id)
        resp = self.session.get(get_status_data_staging_request_endpoint)
        resp.raise_for_status()
        return resp

    @handle_client_exceptions
    def get_upload_token_ingest_for_namespace(self, data_ingest_service_uuid: str, namespace: str):
        """ Get token to upload data for ingest, restricted to a specific namespace.

        :param str data_ingest_service_uuid: The data ingest service UUID.
        :param str namespace: The data identifier namespace.

        :return: A requests response.
        :rtype: requests.models.Response
        """
        get_upload_token_ingest_endpoint = \
            "{api_url}/data/upload/ingest/{data_ingest_service_uuid}/{namespace}".format(
                api_url=self.api_url,
                data_ingest_service_uuid=data_ingest_service_uuid,
                namespace=namespace)

        resp = self.session.get(get_upload_token_ingest_endpoint)
        resp.raise_for_status()
        return resp

    @handle_client_exceptions
    def health(self):
        """ Get the service health.

        :return: A requests response.
        :rtype: requests.models.Response
        """
        health_endpoint = "{api_url}/health".format(api_url=self.api_url)
        resp = self.session.get(health_endpoint)
        resp.raise_for_status()
        return resp

    @handle_client_exceptions
    def list_namespaces(self):
        """ List namespaces.

        :return: A requests response.
        :rtype: requests.models.Response
        """
        list_namespaces_endpoint = "{api_url}/data/list".format(api_url=self.api_url)
        resp = self.session.get(list_namespaces_endpoint)
        resp.raise_for_status()
        return resp

    @handle_client_exceptions
    def list_files_in_namespace(self, namespace: str, name: str, detail: bool = False, filters: str = None,
                                limit: int = 100):
        """ List files in a namespace.

        :param str namespace: The data identifier namespace.
        :param str name: The data identifier name (can be a wildcard, i.e. *).
        :param bool detail: Include details about the file, e.g. type, size
        :param str filters: Filter the results by some criteria.
        :param int limit: Limit the number of results returned.

        :return: A requests response.
        :rtype: requests.models.Response
        """
        list_files_in_namespace_endpoint = "{api_url}/data/list/{namespace}".format(api_url=self.api_url,
                                                                                    namespace=namespace)
        params = {
            "name": name,
            "detail": detail,
            "filters": filters,
            "limit": limit
        }
        resp = self.session.get(list_files_in_namespace_endpoint, params=params)
        resp.raise_for_status()
        return resp

    @handle_client_exceptions
    def list_rules_for_namespace(self, namespace: str, datetime_from: str = "now-1h", datetime_to: str = "now",
                                 limit: int = 1000):
        """ List rules in a namespace.

        :param str namespace: The data identifier namespace (can be all i.e. *).
        :param bool datetime_from: Human readable from datetime.
        :param str filters: Human readable to datetime.
        :param int limit: Limit the number of results returned.

        :return: A requests response.
        :rtype: requests.models.Response
        """
        list_rules_in_namespace_endpoint = "{api_url}/data/rules/{namespace}".format(api_url=self.api_url,
                                                                                     namespace=namespace)
        params = {
            "datetime_from": datetime_from,
            "datetime_to": datetime_to,
            "limit": limit
        }
        resp = self.session.get(list_rules_in_namespace_endpoint, params=params)
        resp.raise_for_status()
        return resp

    @handle_client_exceptions
    def list_rules_for_data_identifier(self, namespace: str, name: str):
        """ List rules for a data identifier.

        :param str namespace: The data identifier namespace.
        :param str name: The data identifier name.

        :return: A requests response.
        :rtype: requests.models.Response
        """
        list_rules_in_namespace_name_endpoint = "{api_url}/data/rules/{namespace}/{name}".format(api_url=self.api_url,
                                                                                                 namespace=namespace,
                                                                                                 name=name)
        params = {
        }
        resp = self.session.get(list_rules_in_namespace_name_endpoint, params=params)
        resp.raise_for_status()
        return resp

    @handle_client_exceptions
    def list_schemas(self):
        """ Get a list of schemas.

        :return: A requests response.
        :rtype: requests.models.Response
        """
        schemas_endpoint = "{api_url}/schemas".format(api_url=self.api_url)
        resp = self.session.get(schemas_endpoint)
        resp.raise_for_status()
        return resp

    @handle_client_exceptions
    def locate_replicas_of_file(self, namespace: str, name: str, ip_address: str = None, sort: str = 'random',
                                limit: int = 10, colocated_services: str = None):
        """ List access points for replicas of a file.

        :param str namespace: The data identifier namespace.
        :param str name: The data identifier name.
        :param str ip_address: The ip address to geolocate the nearest replica to. Leave blank to use the requesting
            client ip (sort == nearest_by_ip only).
        :param str sort: The sorting algorithm to use (random || nearest_by_ip).
        :param int limit: Limit the number of results returned.
        :param str colocated_services: Comma separated list of colocated service types.

        :return: A requests response.
        :rtype: requests.models.Response
        """
        locate_replicas_of_file = "{api_url}/data/locate/{namespace}/{name}".format(api_url=self.api_url,
                                                                                    namespace=namespace,
                                                                                    name=name)
        params = {
            "ip_address": ip_address,
            "sort": sort,
            "limit": limit,
            "colocated_services": colocated_services
        }
        resp = self.session.get(locate_replicas_of_file, params=params)
        resp.raise_for_status()
        return resp

    @handle_client_exceptions
    def make_data_removal_request(self, from_storage_area_uuid: str, dids: [] = []):
        """ Make a data removal request.

        :param str from_storage_area_uuid: The storage area uuid to remove data from.
        :param list dids: The list of DIDs to remove.

        :return: A requests response.
        :rtype: requests.models.Response
        """
        make_data_removal_request_endpoint = "{api_url}/data/remove".format(api_url=self.api_url)
        params = {
            "from_storage_area_uuid": from_storage_area_uuid
        }
        resp = self.session.delete(make_data_removal_request_endpoint, params=params, data=json.dumps(dids))
        resp.raise_for_status()
        return resp

    @handle_client_exceptions
    def make_data_movement_request(self, to_storage_area_uuid: str, lifetime: int, parent_namespace: str = None,
                                   dids: [] = []):
        """ Make a data movement request.

        :param str to_storage_area_uuid: The storage area uuid to move data to.
        :param int lifetime: The lifetime of the data in seconds.
        :param str parent_namespace: The parent container namespace. Defaults to using the first DID's namespace
            (Rucio only).
        :param list dids: The list of DIDs to move.

        :return: A requests response.
        :rtype: requests.models.Response
        """
        make_data_movement_request_endpoint = "{api_url}/data/move".format(api_url=self.api_url)
        params = {
            "to_storage_area_uuid": to_storage_area_uuid,
            "lifetime": lifetime,
            "parent_namespace": parent_namespace
        }
        resp = self.session.post(make_data_movement_request_endpoint, params=params, data=json.dumps(dids))
        resp.raise_for_status()
        return resp

    @handle_client_exceptions
    def make_data_staging_request(self, to_storage_area_uuid: str, lifetime: int, parent_namespace: str = None,
                                  dids: [] = []):
        """ Make a data staging request.

        :param str to_storage_area_uuid: The storage area uuid to move data to.
        :param int lifetime: The lifetime of the data in seconds.
        :param str parent_namespace: The parent container namespace. Defaults to using the first DID's namespace
            (Rucio only).
        :param list dids: The list of DIDs to move.

        :return: A requests response.
        :rtype: requests.models.Response
        """
        make_data_staging_request_endpoint = "{api_url}/data/staging".format(api_url=self.api_url)
        params = {
            "to_storage_area_uuid": to_storage_area_uuid,
            "lifetime": lifetime,
            "parent_namespace": parent_namespace
        }
        resp = self.session.post(make_data_staging_request_endpoint, params=params, data=json.dumps(dids))
        resp.raise_for_status()
        return resp

    @handle_client_exceptions
    def ping(self):
        """ Ping the service.

        :return: A requests response.
        :rtype: requests.models.Response
        """
        ping_endpoint = "{api_url}/ping".format(api_url=self.api_url)
        resp = self.session.get(ping_endpoint)
        resp.raise_for_status()
        return resp

    @handle_client_exceptions
    def set_metadata(self, namespace: str, name: str, metadata: {} = {}):
        """ Set metadata for a data identifier.

        :param str namespace: The data identifier namespace.
        :param str name: A data identifier name.
        :param dict metadata: JSON formatted data.

        :return: A requests response.
        :rtype: requests.models.Response
        """
        set_metadata_endpoint = "{api_url}/metadata/{namespace}/{name}".format(api_url=self.api_url,
                                                                               namespace=namespace,
                                                                               name=name)
        resp = self.session.post(set_metadata_endpoint, data=json.dumps(metadata))
        resp.raise_for_status()
        return resp
