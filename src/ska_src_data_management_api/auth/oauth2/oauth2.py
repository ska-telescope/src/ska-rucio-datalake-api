import abc
import logging
from typing import Union, Optional

import asyncio
from authlib.integrations.requests_client import OAuth2Session
from fastapi import Depends, HTTPException

from ska_src_data_management_api.cache.redis import Cache
from ska_src_data_management_api.common.exceptions import handle_exceptions, ClientNotDefinedError, ClientTokenError, \
    PermissionDenied


class OAuth2ServiceToken(abc.ABC):
    """ A base class for retrieving service tokens. This allows for different clients to be used & parameters to be
    easily modified and restricted, e.g. scope locked storage.[read|write|modify]:/* tokens for download and upload.
    """

    def __init__(self, shortname, default_scopes, audience, client, iam_token_endpoint,
                 try_use_cache: Optional[bool] = True, cache: Optional[Cache] = None,
                 cache_key_prefix: Optional[str] = 'X', additional_scopes: Optional[str] = None,
                 logger=logging.getLogger("uvicorn")):
        self.shortname = shortname
        self.default_scopes = default_scopes
        self.audience = audience
        self.client = client
        self.iam_token_endpoint = iam_token_endpoint
        self.try_use_cache = try_use_cache
        self.cache = cache
        self.cache_key_prefix = cache_key_prefix
        self.additional_scopes = additional_scopes
        self.logger = logger

    @handle_exceptions
    async def get(self, force_renew=False) -> Union[dict, HTTPException]:
        additional_scopes = self.additional_scopes if self.additional_scopes else ''

        scopes = "{} {}".format(self.default_scopes, additional_scopes)

        token = None
        if self.try_use_cache:
            if self.cache is not None:
                self.logger.info("Checking if a cached token exists for service {} with scopes '{}' and "
                                 "audience '{}' ...".format(self.shortname, scopes, self.audience))
                token = self.cache.get_cache_token(scopes=scopes, audiences=self.audience,
                                                   key_prefix=self.cache_key_prefix)
        if token and not force_renew:
            self.logger.info("Found valid cached token.")
        else:
            self.logger.info("Couldn't find a valid cached token or forced renewal requested, requesting new token...")
            try:
                token = self.client.fetch_token(
                    self.iam_token_endpoint,
                    grant_type="client_credentials",
                    audience=self.audience,
                    scope=scopes
                )
                if self.cache is not None:
                    self.cache.set_cache_token(scopes=scopes, audiences=self.audience, token=token,
                                               key_prefix=self.cache_key_prefix)
            except Exception as e:
                raise ClientTokenError(self.__class__.__name__, repr(e))
        return token.get('access_token')


class OAuth2ServiceTokenFactory:
    """ Factory for OAuth2 token retrieval. """
    def __init__(self, rucio_admin_client_params=None, site_capabilities_client_params=None, cache=None, logger=None):
        self.rucio_admin_client_params = rucio_admin_client_params
        self.site_capabilities_client_params = site_capabilities_client_params

        # Instantiate an OAuth2 request session for the rucio admin client.
        self.rucio_admin_client = None
        if rucio_admin_client_params:
            self.rucio_admin_client = OAuth2Session(rucio_admin_client_params.get("client_id"),
                                                    rucio_admin_client_params.get("client_secret"),
                                                    scope=rucio_admin_client_params.get("client_scopes", ""))

        # Instantiate an OAuth2 request session for the site-capabilities-api client.
        self.site_capabilities_client = None
        if site_capabilities_client_params:
            self.site_capabilities_client = OAuth2Session(site_capabilities_client_params.get("client_id"),
                                                          site_capabilities_client_params.get("client_secret"),
                                                          scope=site_capabilities_client_params.get(
                                                              "client_scopes", ""))
        self.cache = cache
        self.logger = logger

    @handle_exceptions
    async def get_rucio_service_token(self, audience: Optional[str] = None,
                                      try_use_cache: Optional[bool] = True,
                                      additional_scopes: Optional[str] = None,
                                      force_renew: Optional[bool] = False):
        if self.rucio_admin_client:
            # avoid circular dependency
            from ska_src_data_management_api.auth.oauth2.rucio import RucioServiceToken
            return (await RucioServiceToken(self, audience=audience, try_use_cache=try_use_cache,
                                           additional_scopes=additional_scopes).get(force_renew=force_renew))
        else:
            raise ClientNotDefinedError("rucio")

    @handle_exceptions
    async def get_site_capabilities_service_token(self, audience: Optional[str] = None,
                                                  try_use_cache: Optional[bool] = True,
                                                  additional_scopes: Optional[str] = None,
                                                  force_renew: Optional[bool] = False):
        if self.site_capabilities_client:
            # avoid circular dependency
            from ska_src_data_management_api.auth.oauth2.site_capabilities_api import SiteCapabilitiesServiceToken
            return (await SiteCapabilitiesServiceToken(self, audience=audience, try_use_cache=try_use_cache,
                                                       additional_scopes=additional_scopes).get(
                force_renew=force_renew))
        else:
            raise ClientNotDefinedError("site-capabilities-api")
