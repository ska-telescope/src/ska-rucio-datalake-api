from typing import Optional

from ska_src_data_management_api.auth.oauth2.oauth2 import OAuth2ServiceToken


class RucioServiceToken(OAuth2ServiceToken):
    """ Retrieve and return a Rucio service token. """
    def __init__(self, token_factory, audience: Optional[str] = None,
                 try_use_cache: Optional[bool] = True, additional_scopes: Optional[str] = None):
        super().__init__(
            shortname='rucio',
            default_scopes=token_factory.rucio_admin_client_params.get("client_scopes"),
            audience=audience if audience else token_factory.rucio_admin_client_params.get("client_audience"),
            client=token_factory.rucio_admin_client,
            iam_token_endpoint=token_factory.rucio_admin_client_params.get('iam_token_endpoint'),
            try_use_cache=try_use_cache,
            cache=token_factory.cache,
            cache_key_prefix='token_rucio',
            additional_scopes=additional_scopes,
            logger=token_factory.logger
        )
