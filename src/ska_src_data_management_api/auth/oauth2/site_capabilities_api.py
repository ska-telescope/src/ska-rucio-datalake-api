from typing import Optional

from ska_src_data_management_api.auth.oauth2.oauth2 import OAuth2ServiceToken


class SiteCapabilitiesServiceToken(OAuth2ServiceToken):
    """ Retrieve and return a Site Capabilities API service token. """
    def __init__(self, token_factory, audience: Optional[str] = None,
                 try_use_cache: Optional[bool] = True, additional_scopes: Optional[str] = None):
        super().__init__(
            shortname='site-capabilities-api',
            default_scopes=token_factory.site_capabilities_client_params.get("client_scopes"),
            audience=audience if audience else token_factory.site_capabilities_client_params.get("client_audience"),
            client=token_factory.site_capabilities_client,
            iam_token_endpoint=token_factory.site_capabilities_client_params.get('iam_token_endpoint'),
            try_use_cache=try_use_cache,
            cache=token_factory.cache,
            cache_key_prefix='token_sc',
            additional_scopes=additional_scopes,
            logger=token_factory.logger
        )
