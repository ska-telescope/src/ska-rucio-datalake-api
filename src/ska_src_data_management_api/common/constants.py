import enum
import requests

from ska_src_data_management_api.common.exceptions import IAMEndpointNotFoundInWellKnown


@enum.unique
class DataMovementStatus(str, enum.Enum):
    """
    Enumeration of states reported back for data movement requests.
    """
    WRAPPING = 'WRAPPING'
    MOVING = 'MOVING'
    READY = 'READY'
    ERROR = 'ERROR'
    UNKNOWN = 'UNKNOWN'


class DataPreparingStatus(str, enum.Enum):
    """
    Enumeration of states reported back for data preparation during data staging requests.
    """
    VERIFYING = 'VERIFYING'
    PREPARING = 'PREPARING'
    READY = 'READY'
    ERROR = 'ERROR'
    UNKNOWN = 'UNKNOWN'


class DataPreparingStrategies(str, enum.Enum):
    """
    Enumeration of data preparation strategies
    """
    SYMLINKS = 'symlinks'


class PrepareData:
    """
    PrepareData related REST endpoints.
    """
    def __init__(self, prepare_data_url=None, user_authz_header=None):
        self.prepare_data_url = prepare_data_url.rstrip('/')
        self.user_authz_header = user_authz_header

    # Endpoints are in the format (method, url).
    #
    # TODO: Confirm endpoint naming structure
    #
    @property
    def prepare_data_stage(self):
        if not self.prepare_data_url:
            return None
        headers = {
            "Content-Type": 'application/json',
            "Authorization": self.user_authz_header
        }
        return 'POST', self.prepare_data_url, headers
    
    @property
    def prepare_data_status(self):
        if not self.prepare_data_url:
            return None
        headers = {
            "Content-Type": 'application/json',
            "Authorization": self.user_authz_header
        }
        return 'GET', "{}/{{task_id}}".format(self.prepare_data_url), headers


class IAM:
    """
    IAM related REST endpoints.
    """
    def __init__(self, client_conf_url=None):
        # Get oidc endpoints from IAM .well_known.
        resp = requests.get(client_conf_url)
        resp.raise_for_status()
        self.client_well_known = resp.json()

    @property
    def iam_endpoint_authorization(self):
        authorization_endpoint = self.client_well_known.get('authorization_endpoint')
        if not authorization_endpoint:
            raise IAMEndpointNotFoundInWellKnown("authorization_endpoint")
        return authorization_endpoint

    @property
    def iam_endpoint_token(self):
        token_endpoint = self.client_well_known.get('token_endpoint')
        if not token_endpoint:
            raise IAMEndpointNotFoundInWellKnown("token_endpoint")
        return token_endpoint

    @property
    def iam_endpoint_introspection(self):
        introspection_endpoint = self.client_well_known.get('introspection_endpoint')
        if not introspection_endpoint:
            raise IAMEndpointNotFoundInWellKnown("introspection_endpoint")
        return introspection_endpoint


class Rucio:
    """
    Rucio related REST endpoints.
    """
    def __init__(self, rucio_host_url=None, client_conf_url=None):
        self.rucio_host_url = rucio_host_url.rstrip('/')

    # Endpoints are in the format (method, url).
    #
    # Uses {{ }} for optional parameters to be evaluated later at request time.
    #
    @property
    def rucio_endpoint_add_did(self):
        if not self.rucio_host_url:
            return None
        headers = {
            "Content-type": 'application/json',
        }
        return 'POST', "{}/dids/{{scope_name}}".format(self.rucio_host_url), headers

    @property
    def rucio_endpoint_add_rules(self):
        if not self.rucio_host_url:
            return None
        headers = {
            "Content-type": 'application/json',
        }
        return 'POST', "{}/rules/".format(self.rucio_host_url), headers

    @property
    def rucio_endpoint_attach_dids(self):
        if not self.rucio_host_url:
            return None
        headers = {
            "Content-type": 'application/json',
        }
        return 'POST', "{}/dids/{{scope_name}}/dids".format(self.rucio_host_url), headers

    @property
    def rucio_endpoint_delete_metadata(self):
        if not self.rucio_host_url:
            return None
        headers = {
            "Content-type": 'application/json',
        }
        return 'DELETE', "{}/dids/{{scope_name}}/meta".format(self.rucio_host_url), headers

    @property
    def rucio_endpoint_delete_rule(self):
        if not self.rucio_host_url:
            return None
        headers = {
            "Content-type": 'application/json',
        }
        return 'DELETE', "{}/rules/{{rule_id}}".format(self.rucio_host_url), headers

    @property
    def rucio_endpoint_get_did(self):
        if not self.rucio_host_url:
            return None
        headers = {
            "Content-type": 'application/json',
        }
        return 'GET', "{}/dids/{{scope_name}}".format(self.rucio_host_url), headers

    @property
    def rucio_endpoint_get_metadata(self):
        if not self.rucio_host_url:
            return None
        headers = {
            "Content-type": 'application/json',
        }
        return 'GET', "{}/dids/{{scope_name}}/meta".format(self.rucio_host_url), headers

    @property
    def rucio_endpoint_get_rule(self):
        if not self.rucio_host_url:
            return None
        headers = {
            "Content-type": 'application/json',
        }
        return 'GET', "{}/rules/{{rule_id}}".format(self.rucio_host_url), headers

    @property
    def rucio_endpoint_list_content(self):
        if not self.rucio_host_url:
            return None
        headers = {
            "Content-type": 'application/json',
        }
        return 'GET', "{}/dids/{{scope_name}}/dids".format(self.rucio_host_url), headers


    @property
    def rucio_endpoint_list_dids(self):
        if not self.rucio_host_url:
            return None
        headers = {
            "Content-type": 'application/json',
        }
        return 'GET', "{}/dids/{{scope}}/dids/search".format(self.rucio_host_url), headers

    @property
    def rucio_endpoint_list_replicas(self):
        if not self.rucio_host_url:
            return None
        headers = {
            "Content-type": 'application/json',
        }
        return 'POST', "{}/replicas/list".format(self.rucio_host_url), headers

    @property
    def rucio_endpoint_list_rules(self):
        if not self.rucio_host_url:
            return None
        headers = {
            "Content-type": 'application/json',
        }
        return 'GET', "{}/dids/{{scope}}/{{name}}/rules".format(self.rucio_host_url), headers


    @property
    def rucio_endpoint_list_scopes(self):
        if not self.rucio_host_url:
            return None
        headers = {
            "Content-type": 'application/json',
        }
        return 'GET', "{}/scopes".format(self.rucio_host_url), headers

    @property
    def rucio_endpoint_ping(self):
        if not self.rucio_host_url:
            return None
        headers = {
            "Content-type": 'application/json',
        }
        return 'GET', "{}/ping".format(self.rucio_host_url), headers

    @property
    def rucio_endpoint_set_metadata(self):
        if not self.rucio_host_url:
            return None
        headers = {
            "Content-type": 'application/json',
        }
        return 'POST', "{}/dids/{{scope_name}}/meta".format(self.rucio_host_url), headers


class RucioDIDTypes(str, enum.Enum):
    """
    Enumeration of Rucio native data identifier types.
    """
    FILE = 'FILE'
    DATASET = 'DATASET'
    CONTAINER = 'CONTAINER'


class RucioRuleStates(str, enum.Enum):
    """
    Enumeration of Rucio rule states.
    """
    OK = 'OK'
    REPLICATING = 'REPLICATING'
    STUCK = 'STUCK'
