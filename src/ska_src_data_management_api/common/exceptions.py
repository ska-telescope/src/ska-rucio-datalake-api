import requests
import traceback
from functools import wraps

from fastapi import HTTPException, status
import geoip2.database


def handle_client_exceptions(func):
    """ Decorator to handle client exceptions. """
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except requests.exceptions.HTTPError as e:
            status_code = e.response.status_code
            detail = f"HTTP error occurred: {e}, response: {e.response.text}"
            raise HTTPException(status_code=status_code, detail=detail)
        except HTTPException as e:
            raise e
        except CustomException as e:
            raise Exception(message=e.message)
        except CustomHTTPException as e:
            raise HTTPException(status_code=e.http_error_status, detail=e.message)
        except Exception as e:
            detail = "General error occurred: {}, traceback: {}".format(
                repr(e), ''.join(traceback.format_tb(e.__traceback__)))
            raise HTTPException(status_code=500, detail=detail)
    return wrapper


def handle_exceptions(func):
    """ Decorator to handle server exceptions. """
    @wraps(func)
    async def wrapper(*args, **kwargs):
        try:
            return await func(*args, **kwargs)
        except requests.exceptions.HTTPError as e:
            status_code = e.response.status_code
            detail = f"HTTP error occurred: {e}, response: {e.response.text}"
            raise HTTPException(status_code=status_code, detail=detail)
        except geoip2.errors.AddressNotFoundError:
            detail = "Address could not be geolocated. Are you localhost?"
            raise HTTPException(status_code=500, detail=detail)
        except HTTPException as e:
            raise e
        except CustomException as e:
            raise Exception(message=e.message)
        except CustomHTTPException as e:
            raise HTTPException(status_code=e.http_error_status, detail=e.message)
        except Exception as e:
            detail = "General error occurred: {}, traceback: {}".format(
                repr(e), ''.join(traceback.format_tb(e.__traceback__)))
            raise HTTPException(status_code=500, detail=detail)
    return wrapper


class CustomException(Exception):
    """ Class that all custom exceptions must inherit in order for exception to be caught by the
    handle_exceptions decorator.
    """
    pass


class IAMEndpointNotFoundInWellKnown(CustomException):
    def __init__(self, endpoint):
        self.message = "Error setting IAM {} endpoint, not found in .well_known".format(endpoint)
        super().__init__(self.message)


class CustomHTTPException(Exception):
    """ Class that all custom HTTP exceptions must inherit in order for exception to be caught by
    the handle_exceptions decorator.
    """
    pass


class ClientNotDefinedError(CustomHTTPException):
    def __init__(self, client_name, exception):
        self.message = "Client {} is not available: {}".format(client_name, exception)
        self.http_error_status = status.HTTP_500_INTERNAL_SERVER_ERROR
        super().__init__(self.message)


class ClientTokenError(CustomHTTPException):
    def __init__(self, client_name, exception):
        self.message = "Error getting {} client service token: {}".format(client_name, exception)
        self.http_error_status = status.HTTP_500_INTERNAL_SERVER_ERROR
        super().__init__(self.message)


class DataIngestServiceDisabled(CustomHTTPException):
    def __init__(self, data_ingest_service_uuid):
        self.message = "The data ingest service with uuid {} is marked as disabled.".format(data_ingest_service_uuid)
        self.http_error_status = status.HTTP_400_BAD_REQUEST
        super().__init__(self.message)


class DataIngestServiceNotFound(CustomHTTPException):
    def __init__(self, data_ingest_service_uuid):
        self.message = "Could not find data ingest service with uuid {}".format(data_ingest_service_uuid)
        self.http_error_status = status.HTTP_400_BAD_REQUEST
        super().__init__(self.message)


class DIDNotFound(CustomHTTPException):
    def __init__(self, scope, name):
        self.scope = scope
        self.name = name
        self.message = "The DID {}:{} could not be found".format(scope, name)
        self.http_error_status = status.HTTP_404_NOT_FOUND
        super().__init__(self.message)


class GeolocationDatabaseError(CustomHTTPException):
    def __init__(self, scope, name):
        self.scope = scope
        self.name = name
        self.message = "Error loading the geolocation database."
        self.http_error_status = status.HTTP_500_INTERNAL_SERVER_ERROR
        super().__init__(self.message)


class PermissionDenied(CustomHTTPException):
    def __init__(self, message="You do not have permission to access this resource."):
        self.message = message
        self.http_error_status = status.HTTP_403_FORBIDDEN
        super().__init__(self.message)


class SchemaNotFound(CustomHTTPException):
    def __init__(self, schema):
        self.message = "Schema with name '{}' could not be found".format(schema)
        self.http_error_status = status.HTTP_404_NOT_FOUND
        super().__init__(self.message)


class StorageAreaNotFound(CustomHTTPException):
    def __init__(self, storage_area_uuid):
        self.message = "Could not find the storage area with uuid {}".format(storage_area_uuid)
        self.http_error_status = status.HTTP_400_BAD_REQUEST
        super().__init__(self.message)


class PrepareDataNotFound(CustomHTTPException):
    def __init__(self, message="Could not find a prepare data service"):
        self.message = message
        self.http_error_status = status.HTTP_400_BAD_REQUEST
        super().__init__(self.message)