import abc
import jwt
import requests
from datetime import datetime, timezone
from functools import wraps

import asyncio


class AuthenticatedOAuth2Session(abc.ABC):
    """ A base class for authenticated OAuth2 sessions. """
    def __init__(self, service_token_factory, token_renewal_function):
        self.service_token_factory = service_token_factory
        self.token_renewal_function = token_renewal_function

        self.session = requests.Session()

    async def _ensure_valid_auth_token(self, lifetime_margin_s=60):
        """ Ensure there's a valid authorization header in the request.

        Notes:
            Renews a token if it's nearing expiry.
        """
        access_token = self._get_access_token_from_authz_header()
        if access_token:
            # Get token expiry
            expiry = datetime.fromtimestamp(
                jwt.decode(access_token, options={"verify_signature": False}).get('exp'),
                tz=timezone.utc
            )
            now = datetime.now(tz=timezone.utc)
            time_until_token_expiry_s = (expiry - now).total_seconds()

            # Check if token is about to expire within margin, <lifetime_margin_s>
            if time_until_token_expiry_s < lifetime_margin_s:
                # Force renewal
                await self._update_authz_header(force_renew=True)
        else:
            # No token in header, just get one from the cache (if it exists)
            await self._update_authz_header()

    def _get_access_token_from_authz_header(self):
        """ Extract Authorization header from token, if it exists. """
        authorization_header = self.session.headers.get('Authorization')

        if authorization_header:
            return authorization_header.replace('Bearer ', '')
        return None

    async def _update_authz_header(self, force_renew=False):
        """ Update Authorization header in token. """
        self.session.headers.update({'Authorization': 'Bearer {}'.format(
            (await self.token_renewal_function(force_renew=force_renew)))})

    async def get(self, ensure_valid_token=True):
        if ensure_valid_token:
            await self._ensure_valid_auth_token()
        return self.session

    def get_sync(self, ensure_valid_token=True):
        return asyncio.run(self.get(ensure_valid_token=ensure_valid_token))
