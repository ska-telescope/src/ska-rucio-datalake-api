from ska_src_data_management_api.session.session import AuthenticatedOAuth2Session


class AuthenticatedSiteCapabilitiesAPIOAuth2Session(AuthenticatedOAuth2Session):
    """ A class for authenticated Site Capabilities API OAuth2 sessions. """
    def __init__(self, service_token_factory):
        super().__init__(
            service_token_factory=service_token_factory,
            token_renewal_function=service_token_factory.get_site_capabilities_service_token
        )
