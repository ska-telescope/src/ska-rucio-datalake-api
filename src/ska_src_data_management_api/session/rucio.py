from ska_src_data_management_api.session.session import AuthenticatedOAuth2Session


class AuthenticatedRucioOAuth2Session(AuthenticatedOAuth2Session):
    """ A class for authenticated Rucio OAuth2 sessions. """
    def __init__(self, service_token_factory):
        super().__init__(
            service_token_factory=service_token_factory,
            token_renewal_function=service_token_factory.get_rucio_service_token
        )

    def _get_access_token_from_authz_header(self):
        """ Extract Authorization header from token, if it exists. """
        authorization_header = self.session.headers.get('X-Rucio-Auth-Token')
        if authorization_header:
            return authorization_header
        return None

    async def _update_authz_header(self, force_renew=False):
        """ Update Authorization header in token. """
        self.session.headers.update({'X-Rucio-Auth-Token': '{}'.format(
            (await self.token_renewal_function(force_renew=force_renew)))})
