#!/bin/bash

# get geoip database
if [ -v GEOIP_LICENSE_KEY ]
then
  geoip_database_compressed_sha256_path='/opt/GeoLite2/GeoLite2-City.tar.gz.sha256'
  wget -O $geoip_database_compressed_sha256_path "https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-City&license_key=$GEOIP_LICENSE_KEY&suffix=tar.gz.sha256"

  geoip_database_path_compressed="/opt/GeoLite2/"`cat /opt/GeoLite2/GeoLite2-City.tar.gz.sha256 | awk -F ' ' '{ print $2 }'`
  wget -O $geoip_database_path_compressed "https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-City&license_key=$GEOIP_LICENSE_KEY&suffix=tar.gz"

  cd /opt/GeoLite2
  sha256sum -c $geoip_database_compressed_sha256_path
  if [ $? -eq 0 ]
  then
    echo "Geoip checksum ok."
  else
    echo "Geoip checksum failed."
    exit 1
  fi

  top_level_directory=`tar -tzf $geoip_database_path_compressed | head -1 | cut -f1 -d"/"`
  echo $top_level_directory
  tar -xzvf $geoip_database_path_compressed
  database_filename=`ls $top_level_directory | grep mmdb`
  export GEOIP_DATABASE_PATH=`readlink -f $top_level_directory/$database_filename`
  cd -
fi

export SERVICE_VERSION=`cat VERSION`
export README_MD=`cat README.md`

cd src/ska_src_data_management_api/rest
env

# set the root path for openapi docs (https://fastapi.tiangolo.com/advanced/behind-a-proxy/)
# this should match any proxy path redirect
cmd="server:app --host "0.0.0.0" --port 8080 --reload --reload-dir ../models/ --reload-dir ../client/ --reload-dir ../rest/ --reload-dir ../common/ --reload-dir ../../../etc/ --reload-dir ../cache/ --reload-dir ../auth/ --reload-dir ../session/ --reload-dir ../tasks/ --reload-include *.json"
if [ ! -z "API_ROOT_PATH" -a "$API_ROOT_PATH" != "" ]; then
  cmd+=' --root-path '$API_ROOT_PATH
fi
echo $cmd

echo $cmd | xargs uvicorn
