#!/bin/bash

env

watchmedo auto-restart --directory=./ --pattern=*.py --recursive -- celery -A src.ska_src_data_management_api.tasks worker --loglevel=INFO
