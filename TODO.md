# TODO

- [ ] (common.utility.retry_*) Refactor functions to use aiohttp (requests is synchronous)
- [ ] (tasks.data.prepare_data) Add ability to use a container's metadata to reformat hierarchical data (from the extended client)