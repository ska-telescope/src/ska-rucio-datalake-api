FROM python:3.8-bullseye

USER root

RUN apt-get update && apt-get install --no-install-recommends -y bash nano mc build-essential wget

RUN groupadd user
RUN adduser --system --no-create-home --disabled-password --shell /bin/bash user

COPY --chown=user . /opt/ska-src-data-management-api

RUN cd /opt/ska-src-data-management-api && python3 -m pip install -e .  \
    --extra-index-url https://gitlab.com/api/v4/projects/48060714/packages/pypi/simple \
    --extra-index-url https://gitlab.com/api/v4/projects/44294004/packages/pypi/simple

RUN mkdir /opt/GeoLite2/
RUN chown user /opt/GeoLite2

WORKDIR /opt/ska-src-data-management-api

ENV DISABLE_AUTHENTICATION ''
ENV API_ROOT_PATH ''
ENV IAM_CLIENT_CONF_URL ''
ENV API_IAM_CLIENT_ID ''
ENV API_IAM_CLIENT_SECRET ''
ENV API_IAM_CLIENT_SCOPES ''
ENV API_IAM_CLIENT_AUDIENCE ''
ENV PERMISSIONS_API_URL ''
ENV PERMISSIONS_SERVICE_NAME ''
ENV PERMISSIONS_SERVICE_VERSION ''
ENV SCHEMAS_RELPATH ''
ENV METADATA_SCHEMA_NAME ''
ENV SITE_CAPABILITIES_URL ''
ENV SITE_CAPABILITIES_CLIENT_ID ''
ENV SITE_CAPABILITIES_CLIENT_SECRET ''
ENV SITE_CAPABILITIES_CLIENT_SCOPES ''
ENV SITE_CAPABILITIES_CLIENT_AUDIENCE ''
ENV GEOIP_LICENSE_KEY ''
ENV RUCIO_HOST_URL ''
ENV RUCIO_IAM_TOKEN_ENDPOINT ''
ENV RUCIO_ADMIN_CLIENT_ID ''
ENV RUCIO_ADMIN_CLIENT_SECRET ''
ENV RUCIO_ADMIN_CLIENT_SCOPES ''
ENV RUCIO_ADMIN_CLIENT_AUDIENCE ''
ENV RUCIO_ACCOUNT ''
ENV CACHE_TYPE ''
ENV CACHE_HOST ''
ENV CACHE_PORT ''

ENTRYPOINT ["/bin/bash", "etc/docker/init.sh"]
