SODA
****

.. toctree::
   :maxdepth: 4

.. mermaid:: _static/img/permissions.mmd
    :zoom:
    :caption: Permissions

.. mermaid:: _static/img/soda.mmd
    :zoom:
    :caption: SODA workflow

1. The workflow starts with the user discovering data with colocated SODA resources via an IVOA Datalink resource. It is assumed that the user already has access to a data-management-api user token, ```<data-management-api-user-token>```, and that they belong to the required groups to call the SODA (```/data/soda/...```) endpoint.
    - client_ip_address: a.b.c.d
    - sort: "nearest_by_client"
    - must_include_soda: True
2. The Datalink service finds replicas of data identifiers, referenced by ```{namespace}:{name}```, that are colocated with SODA resources (```{must_include_soda}=True```) ordered by proximity (```{sort}=nearest_by_client```) to a given client IP address, ```{client_ip_address}```.
3. If no data identifier ```{namespace}:{name}``` is found, the Datalink service returns a HTTP 404.
4. If no replicas of data identifier ```{namespace}:{name}``` are found, the Datalink service returns a HTTP 204.
5. If replicas are found for the data identifier, ```{namespace}:{name}```, then the service returns a HTTP 200 with an IVOA compliant XML Datalink response containing links to colocated SODA services routed through the Data Management API.
6. As per standard IVOA data access workflows, the user now queries the link in the Datalink response which routes through the Data Management API in order for permissions to be checked. The ```{soda_service_uuid}``` links the resource provided by the Datalink response with the API so that the API can redirect the user to the same service referenced in the Datalink response.
7. Begin permissions check.
    - route: ```/data/soda/{soda_service_uuid}/{namespace}/{name}```
    - method: GET
    - version: "latest"
    - token: ```<data-management-api-user-token>```
8. The ```<data-management-api-user-token>``` token is introspected for validity and the required group membership to make a request to this endpoint with the given parameters.
    - client_id: ff02aca2-dc21-49d8-998a-337eb8aedf1a
    - client_secret: <redacted>
    - token: <data-management-api-user-token>
9. The token introspection response is sent from IAM.
10. The Permissions API maps user groups to roles, roles to access (RBAC) and makes an access decision.
11. In the event of the user not being authorised, the Permissions API returns an ```{"is_authorised": "no"}``` response to the Data Management API.
12. This unauthorised response is returned to the user.
13. In the event of the user being authorised, the Permissions API returns an ```{"is_authorised": "yes"}``` response to the Data Management API.
14. The Data Management API looks in its cache for a valid token to be used with the Site Capabilities API. This token must have the "site-capabilities-api-service" scope and "site-capabilities-api" audience.
15. If possible, the cache returns a valid token, ```<site-capabilities-api-service-token>```
16. If no such token with required scope and audience exists in the cache, the Data Management API requests one from IAM.
    - client_id: 1f901a38-955f-4198-aedb-002332a3c551
    - client_secret: <redacted>
    - grant_type: "client_credentials"
    - audience: "site-capabilities-api"
    - scope: "openid profile site-capabilities-api-service"
17. On success, a response ```{"token": <site-capabilities-api-service-token>}``` is returned including the token.
18. The token, ```<site-capabilities-api-service-token>``` is stored in the cache.
19. The Data Management API now tries to get path information about the specified SODA resource, ```{soda_service_uuid}```, to redirect to. To do this, it queries the Site Capabilities API `/services` endpoint.
20. The Site Capabilities API gets a list of all services.
21. This list of services is returned to the Data Management API.
22. The Data Management API tries to match the service with same UUID as request, ```{soda_service_uuid}```.
23. If no match exists, a HTTP 404 is returned to the user.
24. If a match exists, the Data Management API attempts to get a storage path restricted token to pass to the SODA service. The cache is first searched for a storage token with the required scopes/audience.
25. If such a token, ```<storage-token>```, exists in the cache, this is returned to the Data Management API.
26. If there is no such token in the cache, a new one is requested from IAM.
27. On success, a response ```{"token": <storage-token>}``` is returned including the token.
28. The token, ```<storage-token>```, is stored in the cache.
29. The redirect path is parsed from matched service.
30. A HTTP 307 redirect is sent to the user.
31.  If set, the user follows the redirect to the SODA resource with the same parameters as the request. The path restricted token may also included as a query parameter, "token".
